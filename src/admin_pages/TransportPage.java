package admin_pages;

import org.openqa.selenium.By;

import admin_tests.AdminLogin;

public class TransportPage extends AdminLogin {
	private By elPersonal=By.id("radPersonal");
	private By elOfficial=By.id("radOfficial");
	
	public void selectVehiclesType(String vType){
		if(vType.equalsIgnoreCase("Personal")){
			driver.findElement(elPersonal).click();
		}else{
			driver.findElement(elOfficial).click();
		}
	}
}
