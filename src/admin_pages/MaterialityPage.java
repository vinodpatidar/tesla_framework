package admin_pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import admin_tests.AdminLogin;

public class MaterialityPage extends AdminLogin {
	private By elFirestName=By.id("txtFirstName");
	private By elLastName=By.id("txtLastName");
	private By elEmail=By.id("txtEmailID");
	private By elJob=By.id("txtJobTitle");
	private By elCompanyName=By.id("txtCompanyName");
	private By elMobile=By.id("txtMobilePhone");
	private By elWorkPhone=By.id("txtWorkPhone");
	private By elAddress=By.id("txtAddress");
	private By elCountry=By.id("ddlCountry");
	private By elState=By.id("ddlState");
	private By elCity=By.id("ddlCity");
	private By elPostalCode=By.id("txtPostalCode");
	private By elRemarks=By.id("txtRemarks");
	private By elTitle=By.id("ddlTitle");
	private By elAddUser=By.id("btnaddUser");
	private By elSaveButton=By.id("btnSave");
	private By elAddContactList=By.id("btnaddcontactlist");//used for mailer as well
	private By elContactListName=By.id("txtlistName");
	private By elBulkUploadButton=By.id("ctl00_ContentPlaceHolder1_fuUpload");
	private By elClickBulkUploadButton=By.id("ctl00_ContentPlaceHolder1_btnUpload");
	private By elCampMailId=By.id("txtMaild");
	private By elCreateCampaignList=By.id("btnCreateCampaign");

	private By elCampaignTitle=By.id("txtCampaignTitle");
	//private By elCampaignSubject=By.id("txtCampaignSubject");
	private By elchkIsPersonalize=By.id("chkIsPersonalize");
	private By elCampaignDescription=By.id("txtCampaignDescription");
	private By elSender=By.id("ddlSender");
	private By elReplyTo=By.id("ddlReply");
	private By elSenderName=By.id("txtSender");
	private By elReminderDays=By.id("ddlReminderDays");
	private By elStartDate=By.id("txtStartDate");
	private By elEndDate=By.id("txtEndDate");
	private By elImportanceToStakeholders=By.id("chklistitem_0");
	private By elInfluenceOnBusiness=By.id("chklistitem_1");
	private By elPotentialFinancialImpacts=By.id("chklistitem_2");
	private By el=By.xpath(".//*[@id='wizard']//ul/li/a[contains(text(),'Next')]");
	private By elEmailSetup=By.id("imgEmailSetup");
	private By elCampaignSubject=By.id("txtCampaignSubject");
	
	
	
	protected void addContactDetail_mendatory(String fName, String lName, String email){
		driver.findElement(elFirestName).sendKeys(fName);
		driver.findElement(elLastName).sendKeys(lName);
		driver.findElement(elEmail).sendKeys(email);
		
		
	}
	
	protected void addContactDetail_other(String job, String company, String cell, String work, String Address, String country,String state, String city, String pin,String Remark ){
		driver.findElement(elJob).sendKeys(job);
		driver.findElement(elCompanyName).sendKeys(company);
		driver.findElement(elMobile).sendKeys(cell);
		driver.findElement(elWorkPhone).sendKeys(work);
		driver.findElement(elAddress).sendKeys(Address);
		driver.findElement(elCountry).sendKeys(country);
		driver.findElement(elState).sendKeys(state);
		driver.findElement(elCity).sendKeys(city);
		driver.findElement(elPostalCode).sendKeys(pin);
		driver.findElement(elRemarks).sendKeys(Remark);
		
		
		
	}
protected void clickAddContact() throws InterruptedException{
	driver.findElement(elAddUser).click();
	
		Thread.sleep(5000);
	
	
}
protected void clickSaveButton() throws InterruptedException{
	driver.findElement(elSaveButton).click();
	
		Thread.sleep(5000);
	
	
}
protected void verifyContactUser(String email){
	List<WebElement> users=driver.findElements(By.xpath(".//*[@id='GridAddressBookData']//table/tbody/tr"));
	if(users.size()>0){
		for (int i = 1; i <= users.size(); i++) {
			String usermail=driver.findElement(By.xpath(".//*[@id='GridAddressBookData']//table/tbody/tr["+i+"]/td[3]")).getText();
			if(usermail.equalsIgnoreCase(email)){
			Assert.assertTrue(true, "Contact user not added");
			}
		}
	}
	
	
	
	
}
protected void clickAddContactList() throws InterruptedException{
	driver.findElement(elAddContactList).click();
	
		Thread.sleep(5000);
	
	
}
protected void contactListName(String name){
	driver.findElement(elContactListName).sendKeys(name);
}
protected void selectContactUserList() throws InterruptedException{
	List<WebElement> userList=driver.findElements(By.xpath(".//*[@id='tblEmailSetup']/tbody/tr/td/input"));
	if(userList.size()>0){
		for (int i = 1; i <= userList.size(); i++) {
			driver.findElement(By.xpath(".//*[@id='tblEmailSetup']/tbody/tr["+i+"]/td/input")).click();
			Thread.sleep(2000);
		}
		
	}else{
		System.out.println("No contact user are available");
	}
}
protected void verifyContactList(String name){
	List<WebElement> lists=driver.findElements(By.xpath(".//*[@id='GridConatctListData']//table/tbody/tr"));
	if(lists.size()>0){
		for (int i = 1; i <= lists.size(); i++) {
			String list=driver.findElement(By.xpath(".//*[@id='GridConatctListData']//table/tbody/tr["+i+"]/td[2]")).getText();
			if(list.equalsIgnoreCase(name)){
			Assert.assertTrue(true, "Contact list not added");
			}
		}
	}
	
	
	
	
}
protected void bulkUploadList(String filepath){
	driver.findElement(elBulkUploadButton).sendKeys(filepath);
}
protected void clickBulkUploadButton(){
	driver.findElement(elClickBulkUploadButton).click();
}
protected void enterCampMailerID(String email){
	driver.findElement(elCampMailId).sendKeys(email);
}
protected void verifyCampMailer(String email){
	List<WebElement> mailers=driver.findElements(By.xpath(".//*[@id='GridMailerListData']//table/tbody/tr"));
	if(mailers.size()>0){
		for (int i = 1; i <= mailers.size(); i++) {
			String usermail=driver.findElement(By.xpath(".//*[@id='GridMailerListData']//table/tbody/tr["+i+"]/td[2]")).getText();
			if(usermail.equalsIgnoreCase(email)){
			Assert.assertTrue(true, "Contact user not added");
			}
		}
	}
	}
protected void clickAddCampaignList() throws InterruptedException{
	driver.findElement(elCreateCampaignList).click();
	Thread.sleep(5000);
}
protected void campaignTitle(String title){
	driver.findElement(elCampaignTitle).sendKeys(title);
}
protected void campaignDesc(String desc){
	driver.findElement(elCampaignDescription).sendKeys(desc);
}
protected void selectSender(String mail){
	try {
		Select s=new Select(driver.findElement(elSender));
		s.selectByVisibleText(mail);
	} catch (NoSuchElementException e) {
		System.out.println("No sender exist");
		e.printStackTrace();
	}
}
protected void replyTo(String mail){
	try {
		Select s=new Select(driver.findElement(elReplyTo));
		s.selectByVisibleText(mail);
	} catch (NoSuchElementException e) {
		System.out.println("Reply mail does not exist");
		e.printStackTrace();
	}
}
protected void senderName(String name){
	driver.findElement(elSenderName).sendKeys(name);
}
protected void startDate(String sDate){
	driver.findElement(elStartDate).sendKeys(sDate);
}
protected void endDate(String eDate){
	driver.findElement(elEndDate).sendKeys(eDate);
}
protected void selectMaterialityArea(){
	driver.findElement(elImportanceToStakeholders).click();
	driver.findElement(elPotentialFinancialImpacts).click();
	driver.findElement(elPotentialFinancialImpacts).click();
}
protected void clickEmailSetup() throws InterruptedException{
	driver.findElement(elEmailSetup).click();
	Thread.sleep(5000);
	
}
}
