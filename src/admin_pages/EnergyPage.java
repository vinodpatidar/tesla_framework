package admin_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.Select;

import admin_tests.AdminLogin;

public class EnergyPage extends AdminLogin {
	private By elUnit=By.id("ddlUnits");
	private By elMarketEF=By.id("chkMarketEmissonFactors");
	private By elPlantName=By.id("ddlPlantName");
	private By elFuelName=By.id("ddlFuelCategory");
	/*private By elEF=By.xpath("");
	private By elEF=By.xpath("");
	private By elEF=By.xpath("");
	private By elEF=By.xpath("");
	private By elEF=By.xpath("");*/
	
	protected void selectPlantName(String Name){
		try {
			Select s=new Select(driver.findElement(elPlantName));
			s.selectByVisibleText(Name);
		} catch (NoSuchElementException e) {
			System.out.println("Plant name is not available");
			e.printStackTrace();
		}
	}
	protected void selectFuelCategory(String f_Category){
		try {
			Select s=new Select(driver.findElement(elPlantName));
			s.selectByVisibleText(f_Category);
		} catch (NoSuchElementException e) {
			System.out.println("Fuel Category is not available");
			e.printStackTrace();
		}
	}
	protected boolean applyMarketBasedEF(String status){
		String s="Yes";
		if(s.equalsIgnoreCase(status)){
			driver.findElement(elMarketEF).click();
		}
		return false;
	}
}
