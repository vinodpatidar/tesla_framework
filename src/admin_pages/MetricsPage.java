package admin_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import admin_tests.AdminLogin;

public class MetricsPage extends AdminLogin {
	private By menu=By.xpath(".//*[@id='bs-example-navbar-collapse-1']/ul/li/a[contains(text(),'Administration')]/b[@class='caret']");
	private By submenu=By.xpath(".//*[@id='bs-example-navbar-collapse-1']/ul/li[@class='dropdown open']/ul/div//li/a[text()='Metrics Category']");
    private By addCategoryButton=By.id("btnaddCategory");
    private By selectCategoryType=By.id("ddlSelectType");
    private By categoryName=By.id("txtCategoryName");
    private By subcategoryName=By.id("txtSubCategoryName");
    private By saveButton=By.id("btnSave");
    private By errMsg=By.ByClassName.xpath(".//*[@class='help-block errormsg']");
    private By ddlcategory=By.id("ddlCategory");
    		
    private By cancel=By.id("btnCancel");
    
    public void clickMainMenu(){
    	driver.findElement(menu).click();
        }
    public void clickSubMenu(){
    	driver.findElement(submenu).click();
        }
    public void clickAddButton(){
    	driver.findElement(addCategoryButton).click();
        }
    public void selectCategory(String category){
    	Select s=new Select(driver.findElement(selectCategoryType));
    	s.selectByVisibleText(category);
        }
    public void selectmainCategory(String category){
    	Select s=new Select(driver.findElement(ddlcategory));
    	s.selectByVisibleText(category);
        }
    public void cateName(String cname){
    	driver.findElement(categoryName).sendKeys(cname);
    }
    public void subcateName(String scname){
    	driver.findElement(subcategoryName).sendKeys(scname);
    }
    public void clickSaveButton(){
    	driver.findElement(saveButton).click();
    }
    public boolean err(){
    	return driver.findElement(errMsg).isDisplayed();
		
    	
    }
    public void clickCancelButton(){
    	driver.findElement(cancel).click();
    }
}
