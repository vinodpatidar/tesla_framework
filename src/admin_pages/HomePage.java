package admin_pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import common.CommonElements;

public class HomePage extends CommonElements{
	//public WebDriver driver;
	String baseUrl,nodeUrl, browser;
	
	
	protected static XSSFWorkbook wb;
	//protected File file = new File("D:\\VINOD\\FigBytesTesting\\FigBytes\\datafile.properties");
	protected File file = new File("D:\\VINOD\\TESLA\\Tesla.xlsx");
	  
	protected FileInputStream fileInput = null;
	
	
@BeforeSuite(alwaysRun=true)
protected void openBrowser() throws InterruptedException, InvalidFormatException, IOException{
	Logger log=Logger.getLogger("Open Browser");
	PropertyConfigurator.configure("./log/log4j.properties");
	
	try {
		fileInput = new FileInputStream(file);
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	}
 wb=new XSSFWorkbook(file);

	XSSFSheet sheet=wb.getSheet("Admin");
	
	//load properties file
	
		//	baseUrl=prop.getProperty("URL");
			nodeUrl=sheet.getRow(5).getCell(3).getStringCellValue();
			String browser =sheet.getRow(4).getCell(1).getStringCellValue();
	
	
//	browser="ie";
//	nodeUrl="http://192.168.2.103:5555/wd/hub";
//	 baseUrl="http://162.254.26.202/FBUAT01/Login.aspx#";
				if(browser.equalsIgnoreCase("FireFox")){
				log.info("Mozilla Firefox is opening");
					//To run on Spoon
					// nodeUrl="http://localhost:4444/wd/hub";
					  
						  //to run on Prashant's Machine
					//  nodeUrl="http://192.168.2.113:5566/wd/hub";
					FirefoxProfile profile=new FirefoxProfile();
					profile.setPreference("browser.download.dir", "C:\\TestData\\Downloads");
					profile.setPreference("browser.download.folderList", 2);
					//profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/zip");
				profile.setPreference("browser.download.downloadDir","C:\\TestData\\Downloads"); 
					profile.setPreference("browser.download.defaultFolder","C:\\TestData\\Downloads"); 
					//profile.setPreference("browser.helperApps.neverAsk.saveToDisk","text/anytext ,text/plain,text/html,application/plain,text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml" );
					profile.setPreference("browser.download.manager.focusWhenStarting", false);
					profile.setPreference("browser.download.manager.closeWhenDone", false);
					profile.setPreference("browser.download.manager.showWhenStarting",false);
				    profile.setPreference("pdfjs.disabled", true);
				    profile.setPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
					profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;"//MIME types Of MS Excel File. 
							+ "application/pdf;" //MIME types Of PDF File.
							+ "application/vnd.openxmlformats-officedocument.wordprocessingml.document;" //MIME types Of MS doc File. 
							+ "text/plain;" //MIME types Of text File. 
							+ "text/csv"); //MIME types Of CSV File. 
					profile.setPreference( "browser.download.manager.showWhenStarting", false );
				
					DesiredCapabilities capability=DesiredCapabilities.firefox();
					  capability.setCapability(FirefoxDriver.PROFILE, profile);
					  capability.setBrowserName("firefox");
					//  capability.setVersion("42.0");
					//  capability.setPlatform(Platform.WIN8_1);
					  //capability.setPlatform(Platform.VISTA);
					//  capability.setCapability(browser., value);
					  
					 // to run on remote Machine
				 //driver=new RemoteWebDriver(new URL(nodeUrl),capability);
					driver=new FirefoxDriver(capability);
					 
			}
			else if(browser.equalsIgnoreCase("IE")){
				log.info("Enternet Explorer is opening");
				System.setProperty("webdriver.ie.driver", "D:\\VINOD\\Selenium Setup\\Browser\\IEDriverServer.exe");
				  DesiredCapabilities capability=DesiredCapabilities.internetExplorer();
				 
				 //fast execution
				 capability.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
				 // capability.setCapability(InternetExplorerDriver.FORCE_CREATE_PROCESS, true);
				 capability.setCapability(InternetExplorerDriver.UNEXPECTED_ALERT_BEHAVIOR, "ignore");
				  capability.setCapability(InternetExplorerDriver.IE_SWITCHES, "-private");
				  capability.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
				//  capability.setCapability(InternetExplorerDriver., value);
				  capability.setBrowserName("internet explorer");
				 capability.setVersion("9.0");
				 capability.setJavascriptEnabled(true);
				// capability.setPlatform(Platform.WIN8_1);
				
				 //to run on remote machine
	//driver=new RemoteWebDriver(new URL(nodeUrl),capability);
		driver=new InternetExplorerDriver(capability);
				
			}
			else if(browser.equalsIgnoreCase("Chrome")){
				log.info("Chrome is opening");
				System.setProperty("webdriver.chrome.driver", "D:\\VINOD\\Selenium Setup\\Browser\\chromedriver.exe");
				DesiredCapabilities capability=DesiredCapabilities.chrome();
			//	capability.setCapability(ChromeOptions.CAPABILITY.., value);
				//capability.setPlatform(Platform.WIN8_1);
				//to run on remote machine
		// driver=new RemoteWebDriver(new URL(nodeUrl),capability);
				  
				driver=new ChromeDriver(capability);
			}
			else{
				log.error("Please write a proper name of browser");
				System.out.println("Please write a proper Name of broswer:- FireFox, IE, Chrome");
			}
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
           
			driver.get(sheet.getRow(4).getCell(3).getStringCellValue());
			System.out.println(sheet.getRow(3).getCell(4).getStringCellValue());
			log.info("URL Loaded");
			 driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			 
}

@AfterSuite(alwaysRun=true)

protected void closeBrowser() throws InterruptedException{
	Logger log=Logger.getLogger("Close Browser");
	PropertyConfigurator.configure("./log/log4j.properties");
	Thread.sleep(5000);
	
driver.close();
	
//	driver.quit();
	log.info("Broswer Closed");
}

	
}
