package admin_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.Select;

import admin_tests.AdminLogin;

public class UserManagementPage extends AdminLogin {
	private By elButtonAddnewUser=By.id("btnaddUser");
	private By elFirstName=By.id("txtFirstName");
	private By elLastName=By.id("txtLastName");
	private By elCountry=By.id("ddlCountry");
	private By elState=By.id("ddlState");
	private By elCity=By.id("ddlCity");
	private By elEmail=By.id("txtEmailOrg");
	private By elPassword=By.id("txtPassword");
	private By elConfirmPwd=By.id("txtCnfrmPassword");
	private By elAddUserRoles=By.id("UserAddRoles");
	private By elFacility=By.id("ddlFacility");
	private By elRoles=By.id("ddlRoles");
	private By elAddMappling=By.xpath("//a[@class='btn btn-primary']");
	private By elButtonSaveRoles=By.xpath("//button[@class='btn btn-primary']");
	private By elSaveUser=By.id("btnSave");
	
	public void clickAddNewUser() throws InterruptedException{
		driver.findElement(elButtonAddnewUser).click();
		Thread.sleep(5000);
	}
	
	public void addFirstName(String f_name){
		driver.findElement(elFirstName).sendKeys(f_name);
	}
	public void addLastName(String l_name){
		driver.findElement(elLastName).sendKeys(l_name);
	}
	public void selectCountry(String country) throws InterruptedException{
		try {
			Select s=new Select(driver.findElement(elCountry));
			s.selectByVisibleText(country);
			Thread.sleep(3000);
		} catch (NoSuchElementException e) {
		System.out.println("Country is not available");
			e.printStackTrace();
		}
	}
	public void selectState(String state) throws InterruptedException{
		try {
			Select s=new Select(driver.findElement(elState));
			s.selectByVisibleText(state);
			Thread.sleep(3000);
		} catch (NoSuchElementException e) {
		System.out.println("State is not available");
			e.printStackTrace();
		}
	}
	public void selectCity(String city) throws InterruptedException{
		try {
			Select s=new Select(driver.findElement(elCity));
			s.selectByVisibleText(city);
			Thread.sleep(3000);
		} catch (NoSuchElementException e) {
		System.out.println("City is not available");
			e.printStackTrace();
		}
	}
	public void addEmail(String mail){
		driver.findElement(elEmail).sendKeys(mail);
	}
	public void addPassword(String pwd){
		driver.findElement(elPassword).sendKeys(pwd);
	}
	public void addCnfPassword(String cnf_pwd){
		driver.findElement(elConfirmPwd).sendKeys(cnf_pwd);
	}
	public void clickAddRoles() throws InterruptedException{
		driver.findElement(elAddUserRoles).click();
		Thread.sleep(5000);
	}
	public void selectFacility(String f){
		try {
			Select s=new Select(driver.findElement(elFacility));
			s.selectByVisibleText(f);
		} catch (NoSuchElementException e) {
		System.out.println("Facility is not available");
			e.printStackTrace();
		}
	}
	public void selectUserRole(String role){
		try {
			Select s=new Select(driver.findElement(elRoles));
			s.selectByVisibleText(role);
		} catch (NoSuchElementException e) {
		System.out.println("Role is not available");
			e.printStackTrace();
		}
	}
	public void clickAddMapping() throws InterruptedException{
		driver.findElement(elAddMappling).click();
		Thread.sleep(2000);
	}
	public void SaveRole() throws InterruptedException{
		driver.findElement(elButtonSaveRoles).click();
		Thread.sleep(5000);
		
	}
	public void SaveUser(){
		driver.findElement(elSaveUser).click();
	}
}
