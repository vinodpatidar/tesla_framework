package admin_pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import admin_tests.AdminLogin;

public class FacilityPage extends AdminLogin {

	private By eladdFacility=By.id("btnaddFacilities");
	private By elFacilityName=By.id("txtFacilityName");
	private By elFacilityType=By.id("ddlFacilityType");
	private By elFacilityAddress1=By.id("txtAddress1");
	private By elFacilityAddress2=By.id("txtAddress2");
	private By elFacilityCountry=By.id("ddlCountry");
	private By elFacilityState=By.id("ddlState");
	private By elFacilityCity=By.id("ddlCity");
	private By elFacilityPostal=By.id("txtPostalCode");
	private By elFacilitySubordinateTo=By.id("ddlSubordinateTo");
	private By elFacilityCurrency=By.id("ddlCurrency");
	private By elVariableCategory=By.id("imgVariableCategory");
	private By elActive=By.id("chkActiveInactive");
	private By elVariableFields=By.id("ddlVariableFields");
	private By eladdButton=By.id("btnAddVariableValue");
	private By elSaveFacility=By.id("btnSave");
	private By elVariableCancel=By.id("btnCancel");
	
	public void clickAddNewFacility() throws InterruptedException{
		driver.findElement(eladdFacility).click();
		Thread.sleep(5000);
	}
	
	public void facilityName(String fName){
		driver.findElement(elFacilityName).sendKeys(fName);
	}
	
	public void selectFacilityType(String fType){
		try {
			Select s=new Select(driver.findElement(elFacilityType));
			s.selectByVisibleText(fType);
		} catch (NoSuchElementException e) {
		System.out.println("Please select Facility type");
			e.printStackTrace();
		}
	}
	public void facilityAddress1(String fAdd1){
		driver.findElement(elFacilityAddress1).sendKeys(fAdd1);
	}
	public void facilityAddress2(String fAdd2){
		driver.findElement(elFacilityAddress2).sendKeys(fAdd2);
	}
	public void selectFacilityCountry(String country){
		try {
			Select s=new Select(driver.findElement(elFacilityCountry));
			s.selectByVisibleText(country);
		} catch (NoSuchElementException e) {
		System.out.println("Country is not available");
			e.printStackTrace();
		}
	}
	public void selectFacilityState(String state){
		try {
			Select s=new Select(driver.findElement(elFacilityState));
			s.selectByVisibleText(state);
		} catch (NoSuchElementException e) {
		System.out.println("State is not available");
			e.printStackTrace();
		}
	}
	public void selectFacilityCity(String city){
		try {
			Select s=new Select(driver.findElement(elFacilityCity));
			s.selectByVisibleText(city);
		} catch (NoSuchElementException e) {
		System.out.println("City is not available");
			e.printStackTrace();
		}
	}
	public void facilityPostal(String fCode){
		driver.findElement(elFacilityPostal).sendKeys(fCode);
	}
	public void selectFacilitySubordinate(String subordinate){
		try {
			Select s=new Select(driver.findElement(elFacilitySubordinateTo));
			s.selectByVisibleText(subordinate);
		} catch (NoSuchElementException e) {
		System.out.println("SubordinateTo is not available");
			e.printStackTrace();
		}
	}
	public void selectFacilityCurrency(String currency){
		try {
			Select s=new Select(driver.findElement(elFacilityCurrency));
			s.selectByVisibleText(currency);
		} catch (NoSuchElementException e) {
		System.out.println("Currency is not available");
			e.printStackTrace();
		}
	}
	public void mapVariableCategory() throws InterruptedException{
		try {
			driver.findElement(elVariableCategory).click();
			driver.switchTo().activeElement();
			List<WebElement> vc=driver.findElements(By.xpath("//table[@id='tblVariableCategory']//tr"));
			for (int i = 1; i <=vc.size(); i++) {
			
				Thread.sleep(3000);
				
				driver.findElement(By.xpath("//table[@id='tblVariableCategory']//tr["+i+"]/td/input[@type='checkbox']")).click();
			}
			Thread.sleep(5000);
			driver.findElement(By.xpath(".//*[@id='myModal']//div[@class='modal-footer']/button[text()='Ok']")).click();
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
		} catch (NoSuchElementException e) {
		System.out.println("Variable category is not available");
			e.printStackTrace();
		}
	}
	public void activeFacility(String y){
		if(y.equalsIgnoreCase("No")){
			driver.findElement(elActive).click();
		}
		
	}
	public void clickSaveFacility(){
	
		driver.findElement(elSaveFacility).click();;
	}
}
