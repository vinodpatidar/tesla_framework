package admin_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.Select;

import admin_tests.AdminLogin;

public class DataReportingPage extends AdminLogin {
	private By elButtonMapUser=By.id("btnaddAddMapUser");
	private By elUser=By.id("ddlUser");
	private By elFacility=By.id("ddlFacility");
	private By elSaveUser=By.id("btnSave");
	/*private By elMapUser=By.id("");
	private By elMapUser=By.id("");
	private By elMapUser=By.id("");
	private By elMapUser=By.id("");*/

	public void clickAddUserMap() throws InterruptedException{
		driver.findElement(elButtonMapUser).click();
		Thread.sleep(5000);
	}
	public void selectUserMap(String user) throws InterruptedException{
		try {
			Select s=new Select(driver.findElement(elUser));
			s.selectByVisibleText(user);
			Thread.sleep(3000);
		} catch (NoSuchElementException e) {
			System.out.println("User does not exist");
			e.printStackTrace();
		}
		
	}
	public void selectuserFacility(String facility) throws InterruptedException{
		try {
			Select s=new Select(driver.findElement(elFacility));
			s.selectByVisibleText(facility);
			Thread.sleep(3000);
		} catch (NoSuchElementException e) {
			System.out.println("Facility does not exist");
			e.printStackTrace();
		}
		
	}
	public void clickSaveUser(){
		driver.findElement(elSaveUser).click();
	}
}
