package admin_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.Select;

import admin_tests.AdminLogin;

public class WasteWaterPage extends AdminLogin {
	private By elCO2EFUnit=By.id("ddlEFUnitAdd");
	private By elDOC=By.id("txtDOC");
	private By elNIE=By.id("txtNIE");
	

	
	
	
	public void addEFUnit(String efUnit){
		Select s=new Select(driver.findElement(elCO2EFUnit));
		try {
			s.selectByVisibleText(efUnit);
		} catch (NoSuchElementException e) {
			System.out.println("CO2 Unit is not available");
			e.printStackTrace();
		}
	}
	public void addDOCValue(String docvalue){
		driver.findElement(elDOC).sendKeys(docvalue);
	}
	public void addNIEValue(String nievalue){
		driver.findElement(elNIE).sendKeys(nievalue);
	}
}
