package admin_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import admin_tests.AdminLogin;

public class VariablePage extends AdminLogin {
	private By variableCategory=By.id("btnaddVariableCategory");
	private By variableCategoryName=By.id("txtVariableCategory");
	private By saveButton=By.id("btnSave");
	private By cancelButton=By.id("btnCancel");
	private By activateButton=By.id("chkActiveInactive");
	private By cfnMsg=By.id("dialog-content-inner");
	private By cfnOk=By.id("dialog-button");
	private By btnAddVariable=By.id("btnAddVariable");
	private By customVariable=By.id("rblCustomVariable");
	private By rblPDVariable=By.id("rblPDVariable");
	private By rblCSVariable=By.id("rblCSVariable");
	private By txtVariableName=By.id("txtVariableName");
	private By ddlUnitType=By.id("ddlUnitType");
	private By ddlUnit=By.id("ddlUnit");
	private By txtDefinition=By.id("txtDefinition");
	private By chkIncidentForm=By.id("chkIncidentForm");
	private By ddlFormType=By.id("ddlFormType");
	private By chkCummulative=By.id("chkCummulative");
	private By ddlFrequency=By.id("ddlFrequency");
	private By ddlVariableCategory=By.id("ddlVariableCategory");
	
	public void clickAddVariableCategory() throws InterruptedException{
		driver.findElement(variableCategory).click();
	
	}
	public void nameVariableCategory(String categoryName){
		driver.findElement(variableCategoryName).sendKeys(categoryName);
	
	}
	public void clickSaveButton(){
		driver.findElement(saveButton).click();
	}
	public void clickCancelButton(){
		driver.findElement(cancelButton).click();
	}
	public boolean activateVCategory(String s){
		if(s.equalsIgnoreCase("No")){
			driver.findElement(activateButton).click();
		}
		return true;}
	public void confirmationMsg(){
		System.out.println(driver.findElement(cfnMsg).getText());
		driver.findElement(cfnOk).click();
		
	}
	//for variable
	public void clickAddVariable() throws InterruptedException{
		driver.findElement(btnAddVariable).click();
		}
	public void selectVariableType(String vType){
		if(vType.equalsIgnoreCase("Custom Variable")){
			driver.findElement(customVariable).click();
		}else
			if(vType.equalsIgnoreCase("Predefined Variable")){
			driver.findElement(rblPDVariable).click();
			
		}else{
			driver.findElement(rblCSVariable).click();
		}
	}
	public void writeVariableName(String vName){
		driver.findElement(txtVariableName).sendKeys(vName);
	}
	public void selectUnitType(String unitType){
		Select uType=new Select(driver.findElement(ddlUnitType));
		try {
			uType.selectByVisibleText(unitType);
		} catch (Exception e) {
			System.out.println("Please select Unit Type");
			e.printStackTrace();
		}
	}
	public void selectUnit(String unit){
		Select uType=new Select(driver.findElement(ddlUnit));
		try {
			uType.selectByVisibleText(unit);
		} catch (Exception e) {
			System.out.println("Please select Unit");
			e.printStackTrace();
		}
	}
	public void variableDefinition(String vDefinition){
		driver.findElement(txtDefinition).sendKeys(vDefinition);
	}
	public void selectFormType(String formType){
		driver.findElement(chkIncidentForm).click();
		Select fType=new Select(driver.findElement(ddlFormType));
		try {
			fType.selectByVisibleText(formType);
		} catch (Exception e) {
			System.out.println("Please select form type");
			e.printStackTrace();
		}
		
	}
	public void selectCummulative(String cmm){
		if(cmm.equalsIgnoreCase("Yes")){
			driver.findElement(chkCummulative).click();
		}
	}
	public void selectDataFrequency(String frequencyType){
		
		Select fType=new Select(driver.findElement(ddlFrequency));
		try {
			fType.selectByVisibleText(frequencyType);
		} catch (Exception e) {
			System.out.println("Please select frequency type");
			e.printStackTrace();
		}
		
	}
public void selectVariableCategory(String cType){
		
		Select fType=new Select(driver.findElement(ddlVariableCategory));
		try {
			fType.selectByVisibleText(cType);
		} catch (Exception e) {
			System.out.println("Please select variable category");
			e.printStackTrace();
		}
		
	}
}
