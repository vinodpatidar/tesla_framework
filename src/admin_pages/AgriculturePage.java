package admin_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import admin_tests.AdminLogin;

public class AgriculturePage extends AdminLogin {

	private By elNumberOfLivestock=By.id("txtLivestockNum");
	private By elLiquidSystemsPercent=By.id("txtLiquidSystemsPercent");
	private By elSolid=By.id("txtSolidstoragenandDrylotPercent");
	private By elPasture=By.id("txtPastureRangePaddockPercent");
	private By elOther=By.id("txtOtherSystems");
	private By elEntericFermentationEF=By.id("txtEF1Add");
	private By elAllSystemsEF=By.id("txtEF2Add");
	private By elLiquidSystemsEF=By.id("txtEF3Add");
	private By elSolidStorageEF =By.id("txtEF4Add");
	private By elPastureRangeEF =By.id("txtEF5Add");
	private By elOtherEF =By.id("txtEF6Add");
	private By elCO2EF_Forestry =By.id("txtEFAdd");
	private By elCO2EF_Unit_Forestry =By.id("ddlEFUnitAdd");
	
	
	
	public void livestockNumber(String livenumber){
		driver.findElement(elNumberOfLivestock).sendKeys(livenumber);
		
	}
	public void liquidSystems(String ls){
		driver.findElement(elLiquidSystemsPercent).sendKeys(ls);
	}
	public void solidSystems(String ss){
		driver.findElement(elSolid).sendKeys(ss);
	}
	public void pastureSystems(String ps){
		driver.findElement(elPasture).sendKeys(ps);
	}
	public void otherSystems(String os){
		driver.findElement(elOther).sendKeys(os);
	}
	public void entericFermentation(String ef){
		driver.findElement(elEntericFermentationEF).clear();
		driver.findElement(elEntericFermentationEF).sendKeys(ef);
	}
	public void allSystems(String as){
		driver.findElement(elAllSystemsEF).clear();
		driver.findElement(elAllSystemsEF).sendKeys(as);
	}
	public void liquidSystemsEF(String ls){
		driver.findElement(elLiquidSystemsEF).clear();
		driver.findElement(elLiquidSystemsEF).sendKeys(ls);
	}
	public void solidStorage(String ss){
		driver.findElement(elSolidStorageEF).clear();
		driver.findElement(elSolidStorageEF).sendKeys(ss);
	}
	public void pastureRange(String pr){
		driver.findElement(elPastureRangeEF).clear();
		driver.findElement(elPastureRangeEF).sendKeys(pr);
	}
	public void other(String ot){
		driver.findElement(elOtherEF).clear();
		driver.findElement(elOtherEF).sendKeys(ot);
	}
	public void CO2EF_Forestry(String ef_forestry){
		driver.findElement(elCO2EF_Forestry).sendKeys(ef_forestry);
	}
	public void CO2EF_Forestry_Unit(String ef_forestry_Unit) throws InterruptedException{
		Select s=new Select(driver.findElement(elCO2EF_Unit_Forestry));
		s.selectByVisibleText(ef_forestry_Unit);
		Thread.sleep(3000);
	}
}
