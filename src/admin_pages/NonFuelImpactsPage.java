package admin_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.Select;

import admin_tests.AdminLogin;

public class NonFuelImpactsPage extends AdminLogin {
	private By elnumberOfUnits=By.id("txtNumberofUnits");
	private By elFullChargesnumberOfUnits=By.id("txtFullCharge");
	private By elLeakageRates=By.id("ddlLeakagerates");
	private By elVoC_Density=By.id("txtDensity");
	private By elVoc_Toluene=By.id("txtToluenePercent");
	private By elVOC=By.id("txtVOCPercent");
	private By elXylene=By.id("txtXylenePercent");
	private By elFormalDehyde=By.id("txtFormaldehydePercent");
	private By elMEK=By.id("txtMEKPercent");
	
	
	public void numberOfUnits(String number_of_units){
		driver.findElement(elnumberOfUnits).sendKeys(number_of_units);
	}
	
	public void fullChargePerUnits(String charge_of_units){
		driver.findElement(elFullChargesnumberOfUnits).sendKeys(charge_of_units);
	}
	
	public void selectLeakageRates(String leakageRate){
		try {
			Select s=new Select(driver.findElement(elLeakageRates));
			s.selectByVisibleText(leakageRate);
		} catch (NoSuchElementException e) {
			System.out.println("No leakage rate available");
			e.printStackTrace();
		}
	}
 public void voc_density(String density){
	 driver.findElement(elVoC_Density).sendKeys(density);
 }
 public void voc_percent(String voc_pc){
	 driver.findElement(elVOC).sendKeys(voc_pc);
 }
 public void toluene(String tln){
	 driver.findElement(elVoc_Toluene).sendKeys(tln);
 }
 public void xylene(String xln){
	 driver.findElement(elXylene).sendKeys(xln);
 }
 public void formalDeHyde(String dehyde){
	 driver.findElement(elFormalDehyde).sendKeys(dehyde);
 }
 public void mek(String mk){
	 driver.findElement(elMEK).sendKeys(mk);
 }
}
