package admin_tests;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.testng.annotations.Test;

import admin_pages.MetricsPage;

public class Metrics extends MetricsPage {
	@Test
	public void addMetricsCategory() throws InterruptedException{
		XSSFSheet sheet=wb.getSheet("Admin");
		clickMainMenu();
		clickSubMenu();
		clickAddButton();
		Thread.sleep(5000);
		selectCategory(sheet.getRow(43).getCell(5).getStringCellValue());
		cateName(sheet.getRow(44).getCell(5).getStringCellValue());
		clickSaveButton();
		acceptAlert();
		Thread.sleep(5000);
		if(err()==true){
			clickCancelButton();
		}
		
		
		
	}
	@Test
	public void addMetricsSubCategory() throws InterruptedException{
		XSSFSheet sheet=wb.getSheet("Admin");
		clickMainMenu();
		clickSubMenu();
		clickAddButton();
		Thread.sleep(5000);
		selectCategory(sheet.getRow(47).getCell(5).getStringCellValue());
		Thread.sleep(5000);
		selectmainCategory(sheet.getRow(48).getCell(5).getStringCellValue());
		subcateName(sheet.getRow(49).getCell(5).getStringCellValue());
		clickSaveButton();
		acceptAlert();
		Thread.sleep(5000);
		
	}
	@Test
	public void deleteMetricsCategory(){
		
	}

}
