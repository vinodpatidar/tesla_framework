package admin_tests;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import admin_pages.TransportPage;
import admin_testcases.TransportTestCases;

public class Transport extends TransportPage implements TransportTestCases {

	@Test
	public void Road_By_Fuel_Owned_Operated_VehiclesTest()
			throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("Transport");
		menu("Data","Road");
		selectActivity("Road (By Fuel) � Owned & Operated Vehicles");
		 clickAddAsset();
		  selectFacility(sheet.getRow(3).getCell(5).getStringCellValue());
		  assetName(sheet.getRow(4).getCell(5).getStringCellValue());
			selectUnit("cubic foot");
			if(isErrorMsgDisplayed()==true){
				clickAddNewEF();
			clickAddNewEF();
			addAuthority();
			addCO2EF("20");
			addCH4EF("30");
			addN2OEF("20");
			selectCO2EFUnit("g / L");
			selectCH4EFUnit("g / L");
			selectN2OEFUnit("g / L");
			saveNewEF();
			}
			avarageCost("200");
			replicate("Yes");
			saveAsset();
			if(isAssetDuplicate()==true)
			{
				closeAssetWindow();
				Assert.fail("Duplicate Emission Factor Exist");
			}
			acceptAlert();
			pageReload();
		
	}

	@Test
	public void Road_By_Fuel_Not_Owned_Operated_VehiclesTest()
			throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("Transport");
		menu("Data","Road");
		selectActivity("Road (By Fuel) � Not Owned & Operated Vehicles");
		 clickAddAsset();
		  selectFacility(sheet.getRow(25).getCell(5).getStringCellValue());
		  assetName(sheet.getRow(26).getCell(5).getStringCellValue());
			selectUnit("cubic foot");
			//Assert.assertFalse(isErrorMsgDisplayed(), "No Emission Factor exist");
			if(isErrorMsgDisplayed()==true){
				clickAddNewEF();
			
			addAuthority();
			addCO2EF("20");
			addCH4EF("30");
			addN2OEF("20");
			selectCO2EFUnit("g / L");
			selectCH4EFUnit("g / L");
			selectN2OEFUnit("g / L");
			saveNewEF();
			}
			avarageCost("200");
			replicate("Yes");
			saveAsset();
			if(isAssetDuplicate()==true)
			{
				closeAssetWindow();
				Assert.fail("Duplicate Emission Factor Exist");
			}
			acceptAlert();
			pageReload();
		
	}

	@Test
	public void Road_By_Distance_Owned_Operated_VehiclesTest()
			throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("Transport");
		menu("Data","Road");
		selectActivity("Road (By Distance) � Owned & Operated Vehicles");
		 clickAddAsset();
		  selectFacility(sheet.getRow(47).getCell(5).getStringCellValue());
		  assetName(sheet.getRow(48).getCell(5).getStringCellValue());
			selectUnit("kilometre");
			if(isErrorMsgDisplayed()==true){
				clickAddNewEF();
			clickAddNewEF();
			addAuthority();
			addCO2EF("20");
			addCH4EF("30");
			addN2OEF("20");
			selectCO2EFUnit("kg / km");
			selectCH4EFUnit("kg / km");
			selectN2OEFUnit("kg / km");
			saveNewEF();
			}
			avarageCost("200");
			replicate("Yes");
			saveAsset();
			if(isAssetDuplicate()==true)
			{
				closeAssetWindow();
				Assert.fail("Duplicate Emission Factor Exist");
			}
			acceptAlert();
			pageReload();
		
	}

	@Test
	public void Road_By_Distance_Not_Owned_Operated_VehiclesTest()
			throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("Transport");
		menu("Data","Road");
		selectActivity("Road (By Distance) � Not Owned & Operated Vehicles");
		 clickAddAsset();
		  selectFacility(sheet.getRow(68).getCell(5).getStringCellValue());
		  assetName(sheet.getRow(69).getCell(5).getStringCellValue());
			selectUnit("kilometre");
			clickAddNewEF();
			addAuthority();
			addCO2EF("20");
			addCH4EF("30");
			addN2OEF("20");
			selectCO2EFUnit("kg / km");
			selectCH4EFUnit("kg / km");
			selectN2OEFUnit("kg / km");
			saveNewEF();
			avarageCost("200");
			replicate("Yes");
			saveAsset();
			if(isAssetDuplicate()==true)
			{
				closeAssetWindow();
				Assert.fail("Duplicate Emission Factor Exist");
			}
			acceptAlert();
			pageReload();
		
	}

	@Test
	public void delete_Road_By_Fuel_Owned_Operated_VehiclesTest()
			throws InterruptedException {
		menu("Data","Road");
		selectActivity("Road (By Fuel) � Owned & Operated Vehicles");
		deleteAsset();
		
	}

	@Test
	public void delete_Road_By_Fuel_Not_Owned_Operated_VehiclesTest()
			throws InterruptedException {
		menu("Data","Road");
		selectActivity("Road (By Fuel) � Not Owned & Operated Vehicles");
		deleteAsset();
		
	}

	@Test
	public void delete_Road_By_Distance_Owned_Operated_VehiclesTest()
			throws InterruptedException {
		menu("Data","Road");
		selectActivity("Road (By Distance) � Owned & Operated Vehicles");
		deleteAsset();
		
	}

	@Test
	public void delete_Road_By_Distance_Not_Owned_Operated_VehiclesTest()
			throws InterruptedException {
		menu("Data","Road");
		selectActivity("Road (By Distance) � Not Owned & Operated Vehicles");
		deleteAsset();
		
	}

	@Override
	public void addAirAssetTest() {
		XSSFSheet sheet=wb.getSheet("Transport");
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAirAssetTest() {
		// TODO Auto-generated method stub
		
	}

	@Test
	public void PublicTransportTest() throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("Transport");
		menu("Data","Public");
		selectActivity("Public Transport");
		 clickAddAsset();
		  selectFacility("TEST FACILITY1");
		  assetName("Test Asset");
			selectUnit("kilometre");
			selectVehiclesType("Personal");
			clickAddNewEF();
			addAuthority();
			addCO2EF("20");
			addCH4EF("30");
			addN2OEF("20");
			selectCO2EFUnit("kg / km");
			selectCH4EFUnit("kg / km");
			selectN2OEFUnit("kg / km");
			saveNewEF();
			avarageCost("200");
			replicate("Yes");
			saveAsset();
			acceptAlert();
			pageReload();
		
	}

	@Test
	public void deletePublicTransportTest() throws InterruptedException {
		menu("Data","Public");
		selectActivity("Public Transport");
		deleteAsset();
		
	}
	
	

}
