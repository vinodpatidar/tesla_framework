package admin_tests;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import admin_pages.MaterialityPage;

public class Materiality extends MaterialityPage {
	@Test
	public void addContactUserTest() throws InterruptedException{
		XSSFSheet sheet=wb.getSheet("Materiality");
		menu("Administration","Contact Form");
		clickAddContact();
		String firstname=sheet.getRow(1).getCell(5).getStringCellValue();
		 String lastname=sheet.getRow(2).getCell(5).getStringCellValue();
		String email=sheet.getRow(3).getCell(5).getStringCellValue();
		
		addContactDetail_mendatory(firstname,lastname,email);
		
		String job=sheet.getRow(4).getCell(5).getStringCellValue();
		String company=sheet.getRow(5).getCell(5).getStringCellValue();
		String cell=sheet.getRow(6).getCell(5).getStringCellValue();
		String work=sheet.getRow(7).getCell(5).getStringCellValue();
		String Address=sheet.getRow(8).getCell(5).getStringCellValue();
		String country=sheet.getRow(9).getCell(5).getStringCellValue();
		String state=sheet.getRow(10).getCell(5).getStringCellValue();
		String city=sheet.getRow(11).getCell(5).getStringCellValue();
		String pin=sheet.getRow(12).getCell(5).getStringCellValue();
		String Remark =sheet.getRow(13).getCell(5).getStringCellValue();
		
		addContactDetail_other(job,company, cell, work, Address, country,state,  city,  pin, Remark );
		clickSaveButton();
		acceptAlert();
		pageReload();
		//verify contact user
		Thread.sleep(10000);
		verifyContactUser(email);
	}
@Test 
public void addContactListTest() throws InterruptedException{
	XSSFSheet sheet=wb.getSheet("Materiality");
	menu("Administration","Contact List");
	clickAddContactList();
	String ListName=sheet.getRow(20).getCell(5).getStringCellValue();
	contactListName(ListName);
	selectContactUserList();
	clickSaveButton();
	acceptAlert();
	pageReload();
	//verify
	Thread.sleep(10000);
	verifyContactList(ListName);
}
@Test
public void addressListBulkUploadTest() throws InterruptedException{
	XSSFSheet sheet=wb.getSheet("Materiality");
	menu("Administration","Address List Bulk Upload");
	String filepath=sheet.getRow(24).getCell(5).getStringCellValue();
	bulkUploadList(filepath);
	clickBulkUploadButton();
	//incomplete
	
}
@Test
public void addCampMailerTest() throws InterruptedException{
	XSSFSheet sheet=wb.getSheet("Materiality");
	menu("Administration","Camp Mailer");	
	clickAddContactList();
	String campMailerId=sheet.getRow(27).getCell(5).getStringCellValue();
	enterCampMailerID(campMailerId);
	clickSaveButton();
	acceptAlert();
	pageReload();
	verifyCampMailer(campMailerId);
	
}
@Test
public void createCampaignListTest() throws InterruptedException{
	XSSFSheet sheet=wb.getSheet("Materiality");
	menu("Administration","Campaign List");	
	clickAddCampaignList();
	
	
}
}
