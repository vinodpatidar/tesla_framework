package admin_tests;

import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

import admin_pages.HomePage;

public class AdminLogin extends HomePage {
	private By txt_id=By.id("ctl00_ContentPlaceHolder1_txtUserName");
	private By txt_pwd=By.id("ctl00_ContentPlaceHolder1_txtPass");
	private By login=By.id("ctl00_ContentPlaceHolder1_btnLogin");
	
	
	protected void enterid(String id){
	driver.findElement(txt_id).sendKeys(id);
		
	}
	protected void enterPwd(String pwd){
		driver.findElement(txt_pwd).sendKeys(pwd);
			
		}
	protected void login(){
		driver.findElement(login).click();
			
		}
	
	@BeforeTest(alwaysRun=true)
	protected void loginTest() throws InterruptedException{
		/*driver=new FirefoxDriver();
		driver.get("http://162.254.26.105/teslaUAT/Accounts/Login.aspx");
		driver.manage().window().maximize();
		Thread.sleep(10000);*/
		enterid("vinod.patidar@binarysemantics.com");
		enterPwd("binary@1");
		login();
	}
	@AfterTest(alwaysRun=true)
	protected void logoutTest() throws InterruptedException{
	driver.navigate().refresh();
	Thread.sleep(5000);
	driver.findElement(By.xpath("//div[@id='bs-example-navbar-collapse-1']//div[@class='logout']/a[contains(text(),'Logout')]")).click();
	//driver.close();
	}
	



}
