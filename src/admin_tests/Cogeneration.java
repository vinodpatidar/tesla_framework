package admin_tests;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.testng.Assert;
import org.testng.annotations.Test;

import admin_pages.EnergyPage;

public class Cogeneration extends EnergyPage {
	@Test
	public void Fuel_Consumed_for_Generating_Co_Gen_Liquid_Owned_SiteTest()
			throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("Cogeneration");
		menu("Data","Co-Generation");
		selectActivity("Fuel Consumed for Generating Co-Gen - Liquid (Owned Site)");
		 clickAddAsset();
		  selectFacility(sheet.getRow(1).getCell(5).getStringCellValue());
		  assetName(sheet.getRow(2).getCell(5).getStringCellValue());
			selectUnit("cubic foot");
			//clickAddNewEF();
			if(isErrorMsgDisplayed()==true){
				clickAddNewEF();
			addAuthority();
			addCO2EF("20");
			addCH4EF("30");
			addN2OEF("20");
			selectCO2EFUnit("g / L");
			selectCH4EFUnit("g / L");
			selectN2OEFUnit("g / L");
			saveNewEF();
			}
			avarageCost("200");
			replicate("Yes");
			saveAsset();
			if(isAssetDuplicate()==true)
			{
				closeAssetWindow();
				Assert.fail("Duplicate Emission Factor Exist");
			}
			acceptAlert();
			 pageReload();
		
	}

	@Test
	public void Fuel_Consumed_for_Generating_Co_Gen_Liquid_Leased_SiteTest()
			throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("Cogeneration");
		menu("Data","Co-Generation");
		selectActivity("Fuel Consumed for Generating Co-Gen - Liquid (Leased Site)");
		 clickAddAsset();
		  selectFacility(sheet.getRow(19).getCell(5).getStringCellValue());
		  assetName(sheet.getRow(20).getCell(5).getStringCellValue());
			selectUnit("cubic foot");
			//clickAddNewEF();
			if(isErrorMsgDisplayed()==true){
				clickAddNewEF();
			addAuthority();
			addCO2EF("20");
			addCH4EF("30");
			addN2OEF("20");
			selectCO2EFUnit("g / L");
			selectCH4EFUnit("g / L");
			selectN2OEFUnit("g / L");
			saveNewEF();
			}
			avarageCost("200");
			replicate("Yes");
			saveAsset();
			if(isAssetDuplicate()==true)
			{
				closeAssetWindow();
				Assert.fail("Duplicate Emission Factor Exist");
			}
			acceptAlert();
			 pageReload();
		
	}

	@Test
	public void Fuel_Consumed_for_Generating_Co_Gen_Solid_Owned_SiteTest()
			throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("Cogeneration");
		menu("Data","Co-Generation");
		selectActivity("Fuel Consumed for Generating Co-Gen - Solid (Owned Site)");
		 clickAddAsset();
		  selectFacility(sheet.getRow(37).getCell(5).getStringCellValue());
		  assetName(sheet.getRow(38).getCell(5).getStringCellValue());
			selectUnit("gram");
		//	clickAddNewEF();
			if(isErrorMsgDisplayed()==true){
				clickAddNewEF();
			addAuthority();
			addCO2EF("20");
			addCH4EF("30");
			addN2OEF("20");
			selectCO2EFUnit("g / g");
			selectCH4EFUnit("g / g");
			selectN2OEFUnit("g / g");
			saveNewEF();
			}
			avarageCost("200");
			replicate("Yes");
			saveAsset();
			if(isAssetDuplicate()==true)
			{
				closeAssetWindow();
				Assert.fail("Duplicate Emission Factor Exist");
			}
			acceptAlert();
			 pageReload();
		
	}

	@Test
	public void Fuel_Consumed_for_Generating_Co_Gen_Solid_Leased_SiteTest()
			throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("Cogeneration");
		menu("Data","Co-Generation");
		selectActivity("Fuel Consumed for Generating Co-Gen - Solid (Leased Site)");
		 clickAddAsset();
		  selectFacility(sheet.getRow(55).getCell(5).getStringCellValue());
		  assetName(sheet.getRow(56).getCell(5).getStringCellValue());
			selectUnit("gram");
		//	clickAddNewEF();
			if(isErrorMsgDisplayed()==true){
				clickAddNewEF();
			addAuthority();
			addCO2EF("20");
			addCH4EF("30");
			addN2OEF("20");
			selectCO2EFUnit("g / g");
			selectCH4EFUnit("g / g");
			selectN2OEFUnit("g / g");
			saveNewEF();
			}
			avarageCost("200");
			replicate("Yes");
			saveAsset();
			if(isAssetDuplicate()==true)
			{
				closeAssetWindow();
				Assert.fail("Duplicate Emission Factor Exist");
			}
			acceptAlert();
			 pageReload();
		
	}

	@Test
	public void delete_Fuel_Consumed_for_Generating_Co_Gen_Liquid_Owned_SiteTest()
			throws InterruptedException {
		menu("Data","Co-Generation");
		selectActivity("Fuel Consumed for Generating Co-Gen - Liquid (Owned Site)");
		deleteAsset();
		
	}

	@Test
	public void delete_Fuel_Consumed_for_Generating_Co_Gen_Liquid_Leased_SiteTest()
			throws InterruptedException {
		menu("Data","Co-Generation");
		selectActivity("Fuel Consumed for Generating Co-Gen - Liquid (Leased Site)");
		deleteAsset();
		
	}

	@Test
	public void delete_Fuel_Consumed_for_Generating_Co_Gen_Solid_Owned_SiteTest()
			throws InterruptedException {
		menu("Data","Co-Generation");
		selectActivity("Fuel Consumed for Generating Co-Gen - Solid (Owned Site)");
		deleteAsset();
	}

	@Test
	public void delete_Fuel_Consumed_for_Generating_Co_Gen_Solid_Leased_SiteTest()
			throws InterruptedException {
		menu("Data","Co-Generation");
		selectActivity("Fuel Consumed for Generating Co-Gen - Solid (Leased Site)");
		deleteAsset();
		
	}
	
}
