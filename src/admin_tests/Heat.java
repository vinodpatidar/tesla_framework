package admin_tests;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.testng.Assert;
import org.testng.annotations.Test;

import admin_pages.EnergyPage;

public class Heat extends EnergyPage {
	@Test
	public void Fuel_Consumed_for_Generating_Heat_Liquid_Owned_SiteTest()
			throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("Heat");
		menu("Data","Heat");
		selectActivity("Fuel Consumed for Generating Heat - Liquid (Owned Site)");
		 clickAddAsset();
		  selectFacility(sheet.getRow(1).getCell(5).getStringCellValue());
		  assetName(sheet.getRow(2).getCell(5).getStringCellValue());
			selectUnit("cubic foot");
			//clickAddNewEF();
			if(isErrorMsgDisplayed()==true){
				clickAddNewEF();
			addAuthority();
			addCO2EF("20");
			addCH4EF("30");
			addN2OEF("20");
			selectCO2EFUnit("g / L");
			selectCH4EFUnit("g / L");
			selectN2OEFUnit("g / L");
			saveNewEF();
			}
			avarageCost("200");
			replicate("Yes");
			saveAsset();
			if(isAssetDuplicate()==true)
			{
				closeAssetWindow();
				Assert.fail("Duplicate Emission Factor Exist");
			}
			acceptAlert();
			 pageReload();
		
	}

	@Test
	public void Fuel_Consumed_for_Generating_Heat_Liquid_Leased_SiteTest()
			throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("Heat");
		menu("Data","Heat");
		selectActivity("Fuel Consumed for Generating Heat - Liquid (Leased Site)");
		 clickAddAsset();
		  selectFacility(sheet.getRow(19).getCell(5).getStringCellValue());
		  assetName(sheet.getRow(20).getCell(5).getStringCellValue());
			selectUnit("cubic foot");
			//clickAddNewEF();
			if(isErrorMsgDisplayed()==true){
				clickAddNewEF();
			addAuthority();
			addCO2EF("20");
			addCH4EF("30");
			addN2OEF("20");
			selectCO2EFUnit("g / L");
			selectCH4EFUnit("g / L");
			selectN2OEFUnit("g / L");
			saveNewEF();
			}
			avarageCost("200");
			replicate("Yes");
			saveAsset();
			if(isAssetDuplicate()==true)
			{
				closeAssetWindow();
				Assert.fail("Duplicate Emission Factor Exist");
			}
			acceptAlert();
			 pageReload();
		
	}

		@Test
	public void Fuel_Consumed_for_Generating_Heat_Solid_Owned_SiteTest()
			throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("Heat");
		menu("Data","Heat");
		selectActivity("Fuel Consumed for Generating Heat- Solid (Owned Site)");
		 clickAddAsset();
		  selectFacility(sheet.getRow(37).getCell(5).getStringCellValue());
		  assetName(sheet.getRow(38).getCell(5).getStringCellValue());
			selectUnit("gram");
			//clickAddNewEF();
			if(isErrorMsgDisplayed()==true){
				clickAddNewEF();
			addAuthority();
			addCO2EF("20");
			addCH4EF("30");
			addN2OEF("20");
			selectCO2EFUnit("g / g");
			selectCH4EFUnit("g / g");
			selectN2OEFUnit("g / g");
			saveNewEF();
			}
			avarageCost("200");
			replicate("Yes");
			saveAsset();
			if(isAssetDuplicate()==true)
			{
				closeAssetWindow();
				Assert.fail("Duplicate Emission Factor Exist");
			}
			acceptAlert();
			 pageReload();
		
	}

		@Test
		public void Fuel_Consumed_for_Generating_Heat_Solid_Leased_SiteTest()
				throws InterruptedException {
			XSSFSheet sheet=wb.getSheet("Heat");
			menu("Data","Heat");
			selectActivity("Fuel Consumed for Generating Heat - Solid (Leased Site)");
			 clickAddAsset();
			  selectFacility(sheet.getRow(55).getCell(5).getStringCellValue());
			  assetName(sheet.getRow(56).getCell(5).getStringCellValue());
				selectUnit("gram");
			//	clickAddNewEF();
				if(isErrorMsgDisplayed()==true){
					clickAddNewEF();
				addAuthority();
				addCO2EF("20");
				addCH4EF("30");
				addN2OEF("20");
				selectCO2EFUnit("g / g");
				selectCH4EFUnit("g / g");
				selectN2OEFUnit("g / g");
				saveNewEF();
				}
				avarageCost("200");
				replicate("Yes");
				saveAsset();
				if(isAssetDuplicate()==true)
				{
					closeAssetWindow();
					Assert.fail("Duplicate Emission Factor Exist");
				}
				acceptAlert();
				 pageReload();
			
		}

	@Test
	public void Heat_Purchased_District_Grid_Heat_Owned_SiteTest()
			throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("Heat");
		menu("Data","Heat");
		selectActivity("Heat Purchased � District/Grid Heat  (Owned Site)");
		 clickAddAsset();
		  selectFacility(sheet.getRow(73).getCell(5).getStringCellValue());
		  assetName(sheet.getRow(74).getCell(5).getStringCellValue());
			selectUnit("calorie");
		//	clickAddNewEF();
			if(isErrorMsgDisplayed()==true){
				clickAddNewEF();
			addAuthority();
			addCO2EF("20");
			addCH4EF("30");
			addN2OEF("20");
			selectCO2EFUnit("g / Cal");
			selectCH4EFUnit("g / Cal");
			selectN2OEFUnit("g / Cal");
			saveNewEF();
			}
			avarageCost("200");
			replicate("Yes");
			saveAsset();
			if(isAssetDuplicate()==true)
			{
				closeAssetWindow();
				Assert.fail("Duplicate Emission Factor Exist");
			}
			acceptAlert();
			 pageReload();
		
	}

	@Test
	public void Heat_Purchased_District_Grid_Heat_Leased_SiteTest()
			throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("Heat");
		menu("Data","Heat");
		selectActivity("Heat Purchased � District/Grid Heat  (Leased Site)");
		 clickAddAsset();
		  selectFacility(sheet.getRow(91).getCell(5).getStringCellValue());
		  assetName(sheet.getRow(92).getCell(5).getStringCellValue());
			selectUnit("calorie");
		//	clickAddNewEF();
			if(isErrorMsgDisplayed()==true){
				clickAddNewEF();
			addAuthority();
			addCO2EF("20");
			addCH4EF("30");
			addN2OEF("20");
			selectCO2EFUnit("g / Cal");
			selectCH4EFUnit("g / Cal");
			selectN2OEFUnit("g / Cal");
			saveNewEF();
			}
			avarageCost("200");
			replicate("Yes");
			saveAsset();
			if(isAssetDuplicate()==true)
			{
				closeAssetWindow();
				Assert.fail("Duplicate Emission Factor Exist");
			}
			acceptAlert();
			 pageReload();
		
	}

	@Test
	public void delete_Fuel_Consumed_for_Generating_Heat_Liquid_Owned_SiteTest()
			throws InterruptedException {
		menu("Data","Heat");
		selectActivity("Fuel Consumed for Generating Heat - Liquid (Owned Site)");
		deleteAsset();
		
	}

	@Test
	public void delete_Heat_Purchased_District_Grid_Heat_Owned_SiteTest()
			throws InterruptedException {
		menu("Data","Heat");
		selectActivity("Heat Purchased � District/Grid Heat  (Owned Site)");
		deleteAsset();
		
	}

	@Test
	public void delete_Fuel_Consumed_for_Generating_Heat_Liquid_Leased_SiteTest()
			throws InterruptedException {
		menu("Data","Heat");
		selectActivity("Fuel Consumed for Generating Heat - Liquid (Leased Site)");
		deleteAsset();
		
	}

	@Test
	public void delete_Heat_Purchased_District_Grid_Heat_Leased_SiteTest()
			throws InterruptedException {
		menu("Data","Heat");
		selectActivity("Heat Purchased � District/Grid Heat  (Leased Site)");
		deleteAsset();
		
	}

	@Test
	public void delete_Fuel_Consumed_for_Generating_Heat_Solid_Owned_SiteTest()
			throws InterruptedException {
		menu("Data","Heat");
		selectActivity("Fuel Consumed for Generating Heat- Solid (Owned Site)");
		deleteAsset();
		
	}

	@Test
	public void delete_Fuel_Consumed_for_Generating_Heat_Solid_Leased_SiteTest()
			throws InterruptedException {
		menu("Data","Heat");
		selectActivity("Fuel Consumed for Generating Heat - Solid (Leased Site)");
		deleteAsset();
		
	}


}
