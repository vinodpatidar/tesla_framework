package admin_tests;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import admin_pages.EnergyPage;
import admin_testcases.EnergyTestCases;

public class Electricity extends EnergyPage {
//	XSSFSheet sheet=wb.getSheet("Admin");
	//String facility="YUMA PLANT";
//String a=sheet.getRow(0).getCell(0).getStringCellValue();
	@Test
	public void Fuel_Consumed_for_Generating_Electricity_Liquid_Owned_SiteTest()
			throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("Electricity");
		menu("Data","Electricity");
		selectActivity("Fuel Consumed for Generating Electricity - Liquid (Owned Site)");
		 clickAddAsset();
		 String facility=sheet.getRow(5).getCell(5).getStringCellValue();
		  selectFacility(facility);
		  String asset_name=sheet.getRow(6).getCell(5).getStringCellValue();
		  assetName(asset_name);
		selectUnit(sheet.getRow(8).getCell(5).getStringCellValue());
	
			if(isErrorMsgDisplayed()==true)
			{
				clickAddNewEF();
			addAuthority();
			addCO2EF("20");
			addCH4EF("30");
			addN2OEF("20");
			selectCO2EFUnit("g / L");
			selectCH4EFUnit("g / L");
			selectN2OEFUnit("g / L");
			saveNewEF();
			}
			avarageCost(sheet.getRow(10).getCell(5).getStringCellValue());
			replicate(sheet.getRow(9).getCell(5).getStringCellValue());
			saveAsset();
			if(isAssetDuplicate()==true)
			{
				closeAssetWindow();
				Assert.fail("Duplicate Emission Factor Exist");
			}
			
			acceptAlert();
			 pageReload();
			 verifyAsset(asset_name,facility);
	}

	@Test
	public void Fuel_Consumed_for_Generating_Electricity_Liquid_Leased_SiteTest()
			throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("Electricity");
		menu("Data","Electricity");
		selectActivity("Fuel Consumed for Generating Electricity - Liquid (Leased Site)");
		 clickAddAsset();
		  selectFacility(sheet.getRow(23).getCell(5).getStringCellValue());
		  assetName(sheet.getRow(24).getCell(5).getStringCellValue());
			selectUnit(sheet.getRow(26).getCell(5).getStringCellValue());
			if(isErrorMsgDisplayed()==true){
				clickAddNewEF();
			clickAddNewEF();
			addAuthority();
			addCO2EF("20");
			addCH4EF("30");
			addN2OEF("20");
			selectCO2EFUnit("g / L");
			selectCH4EFUnit("g / L");
			selectN2OEFUnit("g / L");
			saveNewEF();
			}
			avarageCost("200");
			replicate("Yes");
			saveAsset();
			if(isAssetDuplicate()==true)
			{
				closeAssetWindow();
				Assert.fail("Duplicate Emission Factor Exist");
			}
			acceptAlert();
			verifyEF();
			 pageReload();
		
	}

	@Test
	public void Fuel_Consumed_for_Generating_Electricity_Solid_Owned_SiteTest()
			throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("Electricity");
		menu("Data","Electricity");
		selectActivity("Fuel Consumed for Generating Electricity - Solid (Owned Site)");
		 clickAddAsset();
		  selectFacility(sheet.getRow(59).getCell(5).getStringCellValue());
		  assetName(sheet.getRow(60).getCell(5).getStringCellValue());
			selectUnit(sheet.getRow(65).getCell(5).getStringCellValue());
			if(isErrorMsgDisplayed()==true){
				clickAddNewEF();
			clickAddNewEF();
			addAuthority();
			addCO2EF("20");
			addCH4EF("30");
			addN2OEF("20");
			selectCO2EFUnit("g / g");
			selectCH4EFUnit("g / g");
			selectN2OEFUnit("g / g");
			saveNewEF();
			}
			avarageCost("200");
			replicate("Yes");
			saveAsset();
			if(isAssetDuplicate()==true)
			{
				closeAssetWindow();
				Assert.fail("Duplicate Emission Factor Exist");
			}
			acceptAlert();
			 pageReload();
		
		
	}

	@Test
	public void Fuel_Consumed_for_Generating_Electricity_Solid_Leased_SiteTest()
			throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("Electricity");
		menu("Data","Electricity");
		selectActivity("Fuel Consumed for Generating Electricity - Solid (Leased Site)");
		 clickAddAsset();
		  selectFacility(sheet.getRow(41).getCell(5).getStringCellValue());
		  assetName(sheet.getRow(42).getCell(5).getStringCellValue());
			selectUnit(sheet.getRow(44).getCell(5).getStringCellValue());
			if(isErrorMsgDisplayed()==true){
				clickAddNewEF();
			clickAddNewEF();
			addAuthority();
			addCO2EF("20");
			addCH4EF("30");
			addN2OEF("20");
			selectCO2EFUnit("g / g");
			selectCH4EFUnit("g / g");
			selectN2OEFUnit("g / g");
			saveNewEF();
			}
			avarageCost("200");
			replicate("Yes");
			saveAsset();
			if(isAssetDuplicate()==true)
			{
				closeAssetWindow();
				Assert.fail("Duplicate Emission Factor Exist");
			}
			acceptAlert();
			 pageReload();
		
		
	}
		
	

	@Test
	public void Electricity_Purchased_from_Grid_Owned_SiteTest()
			throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("Electricity");
		menu("Data","Electricity");
		selectActivity("Electricity Purchased from Grid (Owned Site)");
		 clickAddAsset();
		  selectFacility(sheet.getRow(77).getCell(5).getStringCellValue());
		  assetName(sheet.getRow(78).getCell(5).getStringCellValue());
			selectUnit("calorie");
			if(isErrorMsgDisplayed()==true){
				clickAddNewEF();
			clickAddNewEF();
			addAuthority();
			addCO2EF("20");
			addCH4EF("30");
			addN2OEF("20");
			selectCO2EFUnit("g / Cal");
			selectCH4EFUnit("g / Cal");
			selectN2OEFUnit("g / Cal");
			saveNewEF();
			}
			avarageCost("200");
			replicate("Yes");
			saveAsset();
			if(isAssetDuplicate()==true)
			{
				closeAssetWindow();
				Assert.fail("Duplicate Emission Factor Exist");
			}
			acceptAlert();
			 pageReload();
		
	}

	@Test(enabled=true)
	public void Electricity_Purchased_from_Grid_Leased_SiteTest()
			throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("Electricity");
		menu("Data","Electricity");
		selectActivity("Electricity Purchased from Grid (Leased Site)");
		 clickAddAsset();
		  selectFacility(sheet.getRow(95).getCell(5).getStringCellValue());
		  assetName(sheet.getRow(96).getCell(5).getStringCellValue());
			selectUnit("calorie");
			if(isErrorMsgDisplayed()==true){
				clickAddNewEF();
			clickAddNewEF();
			addAuthority();
			addCO2EF("20");
			addCH4EF("30");
			addN2OEF("20");
			selectCO2EFUnit("g / Cal");
			selectCH4EFUnit("g / Cal");
			selectN2OEFUnit("g / Cal");
			saveNewEF();
			}
			avarageCost("200");
			replicate("Yes");
			saveAsset();
			if(isAssetDuplicate()==true)
			{
				closeAssetWindow();
				Assert.fail("Duplicate Emission Factor Exist");
			}
			acceptAlert();
			 pageReload();
		
	}

	@Test
	public void delete_Fuel_Consumed_for_Generating_Electricity_Liquid_Owned_SiteTest()
			throws InterruptedException {
		menu("Data","Electricity");
		selectActivity("Fuel Consumed for Generating Electricity - Liquid (Owned Site)");
		deleteAsset();
		
	}

	@Test
	public void delete_Fuel_Consumed_for_Generating_Electricity_Liquid_Leased_SiteTest()
			throws InterruptedException {
		menu("Data","Electricity");
		selectActivity("Fuel Consumed for Generating Electricity - Liquid (Leased Site)");
		deleteAsset();
	}

	@Test
	public void delete_Fuel_Consumed_for_Generating_Electricity_Solid_Owned_SiteTest()
			throws InterruptedException {
		menu("Data","Electricity");
		selectActivity("Fuel Consumed for Generating Electricity - Solid (Owned Site)");
		deleteAsset();
		
	}

	@Test
	public void delete_Fuel_Consumed_for_Generating_Electricity_Solid_Leased_SiteTest()
			throws InterruptedException {
		menu("Data","Electricity");
		selectActivity("Fuel Consumed for Generating Electricity - Solid (Leased Site)");
		deleteAsset();
		
	}

	@Test
	public void delete_Electricity_Purchased_from_Grid_Owned_SiteTest()
			throws InterruptedException {
		menu("Data","Electricity");
		selectActivity("Electricity Purchased from Grid (Owned Site)");
		deleteAsset();
		
	}

	@Test
	public void delete_Electricity_Purchased_from_Grid_Leased_SiteTest()
			throws InterruptedException {
		menu("Data","Electricity");
		selectActivity("Electricity Purchased from Grid (Leased Site)");
		deleteAsset();
		
	}

	
	
	
}
