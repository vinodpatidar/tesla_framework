package admin_tests;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.testng.Assert;
import org.testng.annotations.Test;

import admin_pages.WastePage;

public class Waste extends WastePage {
	
	@Test
	public void Add_Asset_Waste_Liquid() throws InterruptedException{
		XSSFSheet sheet=wb.getSheet("Transport");
		menu("Data","Waste");
		selectActivity("Waste - Liquid");
	    clickAddAsset();
		
	    selectFacility(sheet.getRow(25).getCell(5).getStringCellValue());
	
		assetName(sheet.getRow(26).getCell(5).getStringCellValue());
		selectUnit("cubic foot");
	//	clickAddNewEF();
		if(isErrorMsgDisplayed()==true){
			clickAddNewEF();
		addAuthority();
		addCO2EF("10");
		addCH4EF("20");
		addN2OEF("30");
		selectCO2EFUnit("g / L");
		selectCH4EFUnit("g / L");
		selectN2OEFUnit("g / L");
		saveNewEF();
		}
		avarageCost("200");
		replicate("Yes");
		saveAsset();
		if(isAssetDuplicate()==true)
		{
			closeAssetWindow();
			Assert.fail("Duplicate Emission Factor Exist");
		}
		acceptAlert();
		pageReload();
		
	}
	@Test
	public void Add_Asset_Waste_Solid() throws InterruptedException{
		XSSFSheet sheet=wb.getSheet("Transport");
		menu("Data","Waste");
		  selectActivity("Waste - Solid");
	    clickAddAsset();
	  
	    selectFacility(sheet.getRow(3).getCell(5).getStringCellValue());
		
		assetName(sheet.getRow(4).getCell(5).getStringCellValue());
		selectUnit("gram");
		//clickAddNewEF();
		if(isErrorMsgDisplayed()==true){
			clickAddNewEF();
		addAuthority();
		addCO2EF("10");
		addCH4EF("20");
		addN2OEF("30");
		selectCO2EFUnit("g / g");
		selectCH4EFUnit("g / g");
		selectN2OEFUnit("g / g");
		saveNewEF();
		}
		avarageCost("200");
		replicate("Yes");
		saveAsset();
		if(isAssetDuplicate()==true)
		{
			closeAssetWindow();
			Assert.fail("Duplicate Emission Factor Exist");
		}
		acceptAlert();
		pageReload();
		
	}
}
