package admin_tests;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.testng.annotations.Test;

import admin_pages.UserManagementPage;

public class UserManagement extends UserManagementPage {
	/*String first_Name="Test";
	String last_Name="User";
	String country="Canada";
	String state="Ontario";
	String city="NEWONT";
	String email="teslauser2@gmail.com";
	String password="binary@1";
	String cnf_password="binary@1";
	String facility="MEXICO PLANT";
	String user_role1="Contributor";
	String user_role2="Validator";*/
	@Test
	public void addNewUser() throws InterruptedException{
		XSSFSheet sheet=wb.getSheet("Admin");
		menu("Administration","User Management");
		clickAddNewUser();
		addFirstName(sheet.getRow(13).getCell(5).getStringCellValue());
		addLastName(sheet.getRow(14).getCell(5).getStringCellValue());
		selectCountry(sheet.getRow(16).getCell(5).getStringCellValue());
		selectState(sheet.getRow(17).getCell(5).getStringCellValue());
		selectCity(sheet.getRow(18).getCell(5).getStringCellValue());
		addEmail(sheet.getRow(21).getCell(5).getStringCellValue());
		addPassword(sheet.getRow(22).getCell(5).getStringCellValue());
		addCnfPassword(sheet.getRow(23).getCell(5).getStringCellValue());
		clickAddRoles();
		selectFacility(sheet.getRow(24).getCell(5).getStringCellValue());
		selectUserRole(sheet.getRow(25).getCell(5).getStringCellValue());
		clickAddMapping();
		selectUserRole(sheet.getRow(26).getCell(5).getStringCellValue());
		clickAddMapping();
		SaveRole();
		SaveUser();
		
	}

}
