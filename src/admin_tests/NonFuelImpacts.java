package admin_tests;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.testng.Assert;
import org.testng.annotations.Test;

import admin_pages.NonFuelImpactsPage;
import admin_testcases.NonFuelImpactTestCases;

public class NonFuelImpacts extends NonFuelImpactsPage implements
		NonFuelImpactTestCases {

	@Test
	public void material_LiquidTest() throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("NonFuelImpact");
		menu("Data","Materials Used");
		selectActivity("Materials Used - Liquid");
		 clickAddAsset();
		  selectFacility(sheet.getRow(1).getCell(5).getStringCellValue());
		  assetName(sheet.getRow(2).getCell(5).getStringCellValue());
			selectUnit("cubic foot");
			//clickAddNewEF();
			if(isErrorMsgDisplayed()==true){
				clickAddNewEF();
			addAuthority();
			addCO2EF("20");
			addCH4EF("30");
			addN2OEF("20");
			selectCO2EFUnit("g / L");
			selectCH4EFUnit("g / L");
			selectN2OEFUnit("g / L");
			saveNewEF();
			}
			avarageCost("200");
			replicate("Yes");
			saveAsset();
			 if(isAssetDuplicate()==true)
				{
					closeAssetWindow();
					Assert.fail("Duplicate Emission Factor Exist");
				}
			acceptAlert();
			 pageReload();

	}

	@Test
	public void delete_MaterialLiquidTest() throws InterruptedException {
		menu("Data","Materials Used");
		selectActivity("Materials Used - Liquid");
		deleteAsset();

	}

	@Test
	public void material_SolidTest() throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("NonFuelImpact");
		menu("Data","Materials Used");
		selectActivity("Materials Used - Solid");
		 clickAddAsset();
		  selectFacility(sheet.getRow(21).getCell(5).getStringCellValue());
		  assetName(sheet.getRow(22).getCell(5).getStringCellValue());
			selectUnit("gram");
			//clickAddNewEF();
			if(isErrorMsgDisplayed()==true){
				clickAddNewEF();
			addAuthority();
			addCO2EF("20");
			addCH4EF("30");
			addN2OEF("20");
			selectCO2EFUnit("g / g");
			selectCH4EFUnit("g / g");
			selectN2OEFUnit("g / g");
			saveNewEF();
			}
			avarageCost("200");
			replicate("Yes");
			saveAsset();
			 if(isAssetDuplicate()==true)
				{
					closeAssetWindow();
					Assert.fail("Duplicate Emission Factor Exist");
				}
			acceptAlert();
			 pageReload();

	}

	@Test
	public void delete_MaterialSolidTest() throws InterruptedException {
		menu("Data","Materials Used");
		selectActivity("Materials Used - Solid");
		deleteAsset();

	}

	@Test
	public void RefrigerantsTest() throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("NonFuelImpact");
		menu("Data","Refrigerants/Halocarbons");
		selectActivity("Refrigerants/Halocarbons");
		clickAddAsset();
		selectFacility(sheet.getRow(41).getCell(5).getStringCellValue());
		assetName(sheet.getRow(42).getCell(5).getStringCellValue());
		numberOfUnits("20");
		fullChargePerUnits("60");
		selectLeakageRates("Commercial air conditioning (ACs; chillers)");
		selectUnit("gram");
		avarageCost("200");
		replicate("Yes");
		saveAsset();
		 if(isAssetDuplicate()==true)
			{
				closeAssetWindow();
				Assert.fail("Duplicate Emission Factor Exist");
			}
		acceptAlert();
		 pageReload();

	}

	@Test
	public void deleteRefrigerantsTest() throws InterruptedException {
		menu("Data","Refrigerants/Halocarbons");
		selectActivity("Refrigerants/Halocarbons");
		deleteAsset();

	}

	@Test
	public void FertilizersTest() throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("NonFuelImpact");
		menu("Data","Fertilizers Use");
		selectActivity("Fertilizers");
		 clickAddAsset();
		  selectFacility(sheet.getRow(55).getCell(5).getStringCellValue());
		  assetName(sheet.getRow(56).getCell(5).getStringCellValue());
			selectUnit("gram");
		//	clickAddNewEF();
			if(isErrorMsgDisplayed()==true){
				clickAddNewEF();
			addAuthority();
			
			addN2OEF("20");
			
			selectN2OEFUnit("g / g");
			saveNewEF();
			}
			avarageCost("200");
			replicate("Yes");
			saveAsset();
			 if(isAssetDuplicate()==true)
				{
					closeAssetWindow();
					Assert.fail("Duplicate Emission Factor Exist");
				}
			acceptAlert();
			 pageReload();

	}

	@Test
	public void deleteFertilizersTest() throws InterruptedException {
		menu("Data","Fertilizers Use");
		selectActivity("Fertilizers");
		deleteAsset();
	}

	@Test
	public void SF6Test() throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("NonFuelImpact");
		menu("Data","SF6");
		selectActivity("SF6");
		clickAddAsset();
		selectFacility(sheet.getRow(73).getCell(5).getStringCellValue());
		assetName(sheet.getRow(74).getCell(5).getStringCellValue());
		selectUnit("gram");
		avarageCost("200");
		replicate("Yes");
		saveAsset();
		 if(isAssetDuplicate()==true)
			{
				closeAssetWindow();
				Assert.fail("Duplicate Emission Factor Exist");
			}
		acceptAlert();
		 pageReload();

	}

	@Test
	public void deleteSF6Test() throws InterruptedException {
		menu("Data","SF6");
		selectActivity("SF6");
		deleteAsset();
	}

	@Test
	public void EndofLifeSolidTest() throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("NonFuelImpact");
		menu("Data","End of Life");
		selectActivity("End Of Life - Solid");
		 clickAddAsset();
		  selectFacility(sheet.getRow(84).getCell(5).getStringCellValue());
		  assetName(sheet.getRow(85).getCell(5).getStringCellValue());
			selectUnit("gram");
			//clickAddNewEF();
			if(isErrorMsgDisplayed()==true){
				clickAddNewEF();
			addAuthority();
			addCO2EF("20");
			addCH4EF("30");
			addN2OEF("20");
			selectCO2EFUnit("g / g");
			selectCH4EFUnit("g / g");
			selectN2OEFUnit("g / g");
			saveNewEF();
			}
			avarageCost("200");
			replicate("Yes");
			saveAsset();
			 if(isAssetDuplicate()==true)
				{
					closeAssetWindow();
					Assert.fail("Duplicate Emission Factor Exist");
				}
			acceptAlert();
			 pageReload();


	}

	@Test
	public void deleteEndofLifeSolidTest() throws InterruptedException {
		menu("Data","End of Life");
		selectActivity("End Of Life - Solid");
		deleteAsset();

	}

	@Test
	public void EndofLifeLiquidTest() throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("NonFuelImpact");
		menu("Data","End of Life");
		selectActivity("End of Life - Liquid");
		 clickAddAsset();
		  selectFacility(sheet.getRow(104).getCell(5).getStringCellValue());
		  assetName(sheet.getRow(105).getCell(5).getStringCellValue());
			selectUnit("cubic foot");
			//clickAddNewEF();
			if(isErrorMsgDisplayed()==true){
				clickAddNewEF();
			addAuthority();
			addCO2EF("20");
			addCH4EF("30");
			addN2OEF("20");
			selectCO2EFUnit("g / L");
			selectCH4EFUnit("g / L");
			selectN2OEFUnit("g / L");
			saveNewEF();
			}
			avarageCost("200");
			replicate("Yes");
			saveAsset();
			 if(isAssetDuplicate()==true)
				{
					closeAssetWindow();
					Assert.fail("Duplicate Emission Factor Exist");
				}
			acceptAlert();
			 pageReload();

	}

	@Test
	public void deleteEndofLifeLiquidTest() throws InterruptedException {
		menu("Data","End of Life");
		selectActivity("End of Life - Liquid");
		deleteAsset();

	}

	@Test
	public void VOCTest() throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("NonFuelImpact");
		menu("Data","VOCs");
		selectActivity("VOCs");
		 clickAddAsset();
		  selectFacility(sheet.getRow(124).getCell(5).getStringCellValue());
		  assetName(sheet.getRow(125).getCell(5).getStringCellValue());
		  voc_density("20");
		  selectUnit("cubic foot");
		  voc_percent("20");
		  toluene("50");
		  xylene("30");
		  formalDeHyde("20");
		  mek("10");
		  avarageCost("200");
			replicate("Yes");
			saveAsset();
			 if(isAssetDuplicate()==true)
				{
					closeAssetWindow();
					Assert.fail("Duplicate Emission Factor Exist");
				}
			acceptAlert();
			 pageReload();
		  

	}

	@Test
	public void deleteVOCTest() throws InterruptedException {
		menu("Data","VOCs");
		selectActivity("VOCs");
		deleteAsset();

	}

}
