package admin_tests;

import org.testng.annotations.Test;

import admin_pages.WasteWaterPage;

public class WasteWater extends WasteWaterPage {
	@Test
	public void add_Asset_Waste_Water_DOC_Solid_Test() throws InterruptedException{
		menu("Data","Waste water");
		selectActivity("WasteWater DOC - Solid");
	    clickAddAsset();
	    selectFacility("TEST FACILITY1");
	
		assetName("Test Asset");
		selectUnit("gram");
		//clickAddNewEF();
		if(isErrorMsgDisplayed()==true){
			clickAddNewEF();
		addAuthority();
		addCH4EF("20");
		addEFUnit("g / g");
		addN2OEF("30");
		saveNewEF();
		}
		avarageCost("300");
		addDOCValue("20");
		addNIEValue("60");
		saveAsset();
		acceptAlert();
	}
	@Test
	public void delete_Asset_Waste_Water_DOC_Solid_Test() throws InterruptedException{}
	@Test
	public void add_Asset_Waste_Water_DOC_Liquid_Test() throws InterruptedException{
		menu("Data","Waste water");
		selectActivity("WasteWater DOC - Liquid");
	    clickAddAsset();
	    selectFacility("TEST FACILITY1");
	
		assetName("Test Asset");
		selectUnit("litre");
		//clickAddNewEF();
		if(isErrorMsgDisplayed()==true){
			clickAddNewEF();
		addAuthority();
		addCH4EF("20");
		addEFUnit("g / L");
		addN2OEF("30");
		saveNewEF();
		}
		avarageCost("300");
		addDOCValue("20");
		addNIEValue("60");
		saveAsset();
		acceptAlert();
	}
	@Test
	public void delete_Asset_Waste_Water_DOC_Liquid_Test() throws InterruptedException{}
	@Test
	public void add_Asset_Waste_Water_BODTest() throws InterruptedException{
		menu("Data","Waste water");
		selectActivity("WasteWater BOD");
	    clickAddAsset();
	    selectFacility("TEST FACILITY1");
	    assetName("Test Asset");
		selectUnit("litre");
		avarageCost("300");
		saveAsset();
		acceptAlert();
		
	}
	@Test
	public void delete_Add_Asset_Waste_Water_BODTest() throws InterruptedException{
		
	}
	@Test
	public void add_Waste_Water_DOC_LiquidTest() throws InterruptedException{
		menu("Data","Waste water");
		selectActivity("WasteWater BOD");
	    clickAddAsset();
	    selectFacility("TEST FACILITY1");
	    assetName("Test Asset");
		selectUnit("litre");
		avarageCost("300");
		addDOCValue("20");
		addNIEValue("20");
		saveAsset();
		acceptAlert();
	}
	@Test
	public void delete_Waste_Water_DOC_LiquidTest() throws InterruptedException{
		
	}
}
