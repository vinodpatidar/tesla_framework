package admin_tests;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.testng.annotations.Test;

import admin_pages.DataReportingPage;

public class DataReporting extends DataReportingPage {
	//String user="Test User";
	//String facility="TEST FACILITY1";
	@Test
	public void mapReporterTest() throws InterruptedException{
		XSSFSheet sheet=wb.getSheet("Admin");
		menu("Administration","Map Reporters");
		clickAddUserMap();
		selectUserMap(sheet.getRow(74).getCell(5).getStringCellValue());
		selectuserFacility(sheet.getRow(75).getCell(5).getStringCellValue());
		clickSaveUser();
		acceptAlert();
	}
@Test
public void mapValidatorsTest() throws InterruptedException{
	XSSFSheet sheet=wb.getSheet("Admin");
	menu("Administration","Map Validators");
	clickAddUserMap();
	selectUserMap(sheet.getRow(76).getCell(5).getStringCellValue());
	selectuserFacility(sheet.getRow(77).getCell(5).getStringCellValue());
	clickSaveUser();
	acceptAlert();
}
}
