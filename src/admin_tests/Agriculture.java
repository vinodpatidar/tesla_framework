package admin_tests;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.testng.Assert;
import org.testng.annotations.Test;

import admin_pages.AgriculturePage;
import admin_testcases.AgricultureTestCases;

public class Agriculture extends AgriculturePage implements AgricultureTestCases {

	@Test
	public void LivestockTest() throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("Agriculture");
		menu("Data","Livestock");
		selectActivity("Livestock");
		clickAddAsset();
		selectFacility(sheet.getRow(3).getCell(5).getStringCellValue());
		assetName(sheet.getRow(4).getCell(5).getStringCellValue());
        selectUnit("Number of Heads");
        livestockNumber("30");
        liquidSystems("20");
        solidSystems("20");
        pastureSystems("20");
        otherSystems("40");
        if(isErrorMsgDisplayed()==true){
		
        clickAddNewEF();
        addAuthority();
        entericFermentation("10");
        allSystems("12");
        liquidSystemsEF("30");
        solidStorage("10");
        pastureRange("10");
        other("20");
        saveNewEF();
        }
        avarageCost("200");
        saveAsset();
        if(isAssetDuplicate()==true)
		{
			closeAssetWindow();
			Assert.fail("Duplicate Emission Factor Exist");
		}
        acceptAlert();
        pageReload();
	}

	@Override
	public void deleteLivestockTest() throws InterruptedException {
		// TODO Auto-generated method stub
		
	}

	@Test
	public void ForestryTest() throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("Agriculture");
		menu("Data","Forestry");
		selectActivity("Forestry");
		clickAddAsset();
		selectFacility(sheet.getRow(29).getCell(5).getStringCellValue());
		assetName(sheet.getRow(30).getCell(5).getStringCellValue());
        selectUnit("ha");
        
      //  clickAddNewEF();
        if(isErrorMsgDisplayed()==true){
			clickAddNewEF();
        CO2EF_Forestry("300");
        CO2EF_Forestry_Unit("g / ha");
        saveNewEF();
        }
        avarageCost("200");
        replicate("Yes");
        saveAsset();
        if(isAssetDuplicate()==true)
		{
			closeAssetWindow();
			Assert.fail("Duplicate Emission Factor Exist");
		}
        acceptAlert();
        pageReload();
	}

	@Override
	public void deleteForestryTest() throws InterruptedException {
		// TODO Auto-generated method stub
		
	}

	@Test
	public void CropsTest() throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("Agriculture");
		menu("Data","Crops");
		selectActivity("Crops");
		clickAddAsset();
		selectFacility(sheet.getRow(47).getCell(5).getStringCellValue());
		assetName(sheet.getRow(48).getCell(5).getStringCellValue());
        selectUnit("kg");
      //  clickAddNewEF();
        if(isErrorMsgDisplayed()==true){
			clickAddNewEF();
        CO2EF_Forestry("300");
        CO2EF_Forestry_Unit("kg / g");
        saveNewEF();
        }
        avarageCost("200");
        replicate("No");
        saveAsset();
        if(isAssetDuplicate()==true)
		{
			closeAssetWindow();
			Assert.fail("Duplicate Emission Factor Exist");
		}
        acceptAlert();
        pageReload();
	}

	@Override
	public void deleteCropsTest() throws InterruptedException {
		// TODO Auto-generated method stub
		
	}

	@Test
	public void LandUseTest() throws InterruptedException {
		XSSFSheet sheet=wb.getSheet("Agriculture");
		menu("Data","Land Use");
		selectActivity("Land Use");
		clickAddAsset();
		selectFacility(sheet.getRow(63).getCell(5).getStringCellValue());
		assetName(sheet.getRow(64).getCell(5).getStringCellValue());
        selectUnit("ha");
        if(isErrorMsgDisplayed()==true){
			clickAddNewEF();
     //   clickAddNewEF();
        CO2EF_Forestry("300");
        CO2EF_Forestry_Unit("kg / ha");
        saveNewEF();
        }
        avarageCost("200");
        replicate("No");
        saveAsset();
        if(isAssetDuplicate()==true)
		{
			closeAssetWindow();
			Assert.fail("Duplicate Emission Factor Exist");
		}
        acceptAlert();
        pageReload();
		
	}

	@Override
	public void deleteLandUseTest() throws InterruptedException {
		// TODO Auto-generated method stub
		
	}
	

}
