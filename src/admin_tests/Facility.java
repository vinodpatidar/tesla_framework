package admin_tests;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.testng.annotations.Test;

import admin_pages.FacilityPage;

public class Facility extends FacilityPage {
	/*XSSFSheet sheet=wb.getSheet("Admin");
	
	String facility_Name="Abbott Plant";
	String facility_Type="Business Unit";
	String facility_add1="Canada";
	String facility_country="Canada";
	String facility_sate="Ontario";
	String facility_city="NEWONT";
	String subordinate_facility="CALIFORNIA";
	String facility_currency="USD";
	String activate="Yes";*/
	@Test
	public void addFacility() throws InterruptedException{
		XSSFSheet sheet=wb.getSheet("Admin");
		menu("Administration","Facility");
		clickAddNewFacility();
		facilityName(sheet.getRow(29).getCell(5).getStringCellValue());
		selectFacilityType(sheet.getRow(30).getCell(5).getStringCellValue());
		String facility_Type=sheet.getRow(30).getCell(5).getStringCellValue();
		facilityAddress1(sheet.getRow(30).getCell(5).getStringCellValue());
		selectFacilityCountry(sheet.getRow(32).getCell(5).getStringCellValue());
		selectFacilityState(sheet.getRow(33).getCell(5).getStringCellValue());
		selectFacilityCity(sheet.getRow(34).getCell(5).getStringCellValue());
		
		if(!facility_Type.equalsIgnoreCase("Hub")){
			selectFacilitySubordinate(sheet.getRow(36).getCell(5).getStringCellValue());
		}
		
		selectFacilityCurrency(sheet.getRow(38).getCell(5).getStringCellValue());
       activeFacility(sheet.getRow(39).getCell(5).getStringCellValue());
       if(facility_Type.equalsIgnoreCase("Business Unit")||facility_Type.equalsIgnoreCase("Other")){
    	   mapVariableCategory();
       }
  	 clickSaveFacility();
	}

}
