package admin_tests;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.testng.annotations.Test;

import admin_pages.VariablePage;

public class Variable extends VariablePage {
	/*String variableCategory="TestVariableCategory";
	String vtype = "custom variable";
	String variableName="Test Custom Variable";
	String unitType="area";
	String unit="acre";*/
@Test
public void addVariableCategory() throws InterruptedException{
	XSSFSheet sheet=wb.getSheet("Admin");
	menu("Administration","Variable Category");
	clickAddVariableCategory();
	Thread.sleep(5000);
	nameVariableCategory(sheet.getRow(51).getCell(5).getStringCellValue() );
	activateVCategory("Yes");
	clickSaveButton();
	Thread.sleep(5000);
	confirmationMsg();
	
}
@Test
public void addVariable() throws InterruptedException{
	XSSFSheet sheet=wb.getSheet("Admin");
	menu("Administration","Variables");
	Thread.sleep(5000);
	clickAddVariable();
	Thread.sleep(5000);
	String vtype=sheet.getRow(54).getCell(5).getStringCellValue();
	selectVariableType(vtype);
	if(vtype.equalsIgnoreCase("Custom Variable")){
		writeVariableName(sheet.getRow(52).getCell(5).getStringCellValue());
		selectUnitType(sheet.getRow(55).getCell(5).getStringCellValue());
		Thread.sleep(5000);
		selectUnit( sheet.getRow(56).getCell(5).getStringCellValue());
		variableDefinition("Test variable definition");
		selectFormType("Default Form");
		selectDataFrequency("Date Wise");
		selectVariableCategory(sheet.getRow(51).getCell(5).getStringCellValue());
		clickSaveButton();
		Thread.sleep(5000);
		confirmationMsg();
	}else if(vtype.equalsIgnoreCase("Predefined Variable")){
		
		
		
	}else{
		
		
	}
	
	
}

}
