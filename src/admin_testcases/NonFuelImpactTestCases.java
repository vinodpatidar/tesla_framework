package admin_testcases;

public interface NonFuelImpactTestCases {
	//Material Used
	public void material_LiquidTest() throws InterruptedException;
	
	public void delete_MaterialLiquidTest() throws InterruptedException;
	
public void material_SolidTest() throws InterruptedException;
	
	public void delete_MaterialSolidTest() throws InterruptedException;
	//Refrigerants
	public void RefrigerantsTest() throws InterruptedException;

	
	public void deleteRefrigerantsTest() throws InterruptedException;
	
	//Fertilizers
	public void FertilizersTest() throws InterruptedException;
	
	public void deleteFertilizersTest() throws InterruptedException;
	
	//SF6
	public void SF6Test() throws InterruptedException;

	
	public void deleteSF6Test() throws InterruptedException;
	
	//End of Life
		public void EndofLifeSolidTest() throws InterruptedException;
		
		public void deleteEndofLifeSolidTest() throws InterruptedException;
public void EndofLifeLiquidTest() throws InterruptedException;
		
		public void deleteEndofLifeLiquidTest() throws InterruptedException;
		
		
		//VOCs
		public void VOCTest() throws InterruptedException;

		public void deleteVOCTest() throws InterruptedException;
}
