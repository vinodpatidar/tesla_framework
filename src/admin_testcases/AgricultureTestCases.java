package admin_testcases;

public interface AgricultureTestCases {
	//livestock
	public void LivestockTest() throws InterruptedException;
	public void deleteLivestockTest() throws InterruptedException;
	
	//forestry
	public void ForestryTest() throws InterruptedException;
	
	public void deleteForestryTest() throws InterruptedException;
	
	//Crops
	public void CropsTest() throws InterruptedException;
	
	public void deleteCropsTest() throws InterruptedException;
	
	//Land Use
	public void LandUseTest() throws InterruptedException;
	
	public void deleteLandUseTest() throws InterruptedException;
	
}
