package admin_testcases;

public interface WasteTestCases {
	//Asset solid
	public void waste_SolidTest() throws InterruptedException;
	
	public void delete_Waste_SolidTest() throws InterruptedException;
	
	//Asset solid
		public void waste_LiquidTest() throws InterruptedException;
		
		public void delete_Waste_LiquidTest() throws InterruptedException;
	
		//Waste Water
		public void waste_Water_DOC_SolidTest() throws InterruptedException;
		
		public void delete_Waste_Water_DOC_SolidTest() throws InterruptedException;
		public void waste_Water_DOC_LiquidTest() throws InterruptedException;
		
		public void delete_Waste_Water_LiquidTest() throws InterruptedException;
	
	//Waste water BOD
		public void waste_Water_BODTest();
		
		public void delete_Waste_Water_BODTest() throws InterruptedException;

}
