package admin_testcases;

public interface EnergyTestCases {
	//Electricity
	public void Fuel_Consumed_for_Generating_Electricity_Liquid_Owned_SiteTest() throws InterruptedException;
	public void Fuel_Consumed_for_Generating_Electricity_Liquid_Leased_SiteTest() throws InterruptedException;
	public void Fuel_Consumed_for_Generating_Electricity_Solid_Owned_SiteTest() throws InterruptedException;
	public void Fuel_Consumed_for_Generating_Electricity_Solid_Leased_SiteTest() throws InterruptedException;
	public void Electricity_Purchased_from_Grid_Owned_SiteTest() throws InterruptedException;
	public void Electricity_Purchased_from_Grid_Leased_SiteTest() throws InterruptedException;
	
	
	public void delete_Fuel_Consumed_for_Generating_Electricity_Liquid_Owned_SiteTest() throws InterruptedException;
	public void delete_Fuel_Consumed_for_Generating_Electricity_Liquid_Leased_SiteTest() throws InterruptedException;
	public void delete_Fuel_Consumed_for_Generating_Electricity_Solid_Owned_SiteTest() throws InterruptedException;
	public void delete_Fuel_Consumed_for_Generating_Electricity_Solid_Leased_SiteTest() throws InterruptedException;
	public void delete_Electricity_Purchased_from_Grid_Owned_SiteTest() throws InterruptedException;
	public void delete_Electricity_Purchased_from_Grid_Leased_SiteTest() throws InterruptedException;

	
	
	//Heat
	public void Fuel_Consumed_for_Generating_Heat_Liquid_Owned_SiteTest() throws InterruptedException;
	public void Heat_Purchased_District_Grid_Heat_Owned_SiteTest() throws InterruptedException;
	public void Fuel_Consumed_for_Generating_Heat_Liquid_Leased_SiteTest() throws InterruptedException;
	public void Heat_Purchased_District_Grid_Heat_Leased_SiteTest() throws InterruptedException;
	public void Fuel_Consumed_for_Generating_Heat_Solid_Owned_SiteTest() throws InterruptedException;
	public void Fuel_Consumed_for_Generating_Heat_Solid_Leased_SiteTest() throws InterruptedException;
	
	public void delete_Fuel_Consumed_for_Generating_Heat_Liquid_Owned_SiteTest() throws InterruptedException;
	public void delete_Heat_Purchased_District_Grid_Heat_Owned_SiteTest() throws InterruptedException;
	public void delete_Fuel_Consumed_for_Generating_Heat_Liquid_Leased_SiteTest() throws InterruptedException;
	public void delete_Heat_Purchased_District_Grid_Heat_Leased_SiteTest() throws InterruptedException;
	public void delete_Fuel_Consumed_for_Generating_Heat_Solid_Owned_SiteTest() throws InterruptedException;
	public void delete_Fuel_Consumed_for_Generating_Heat_Solid_Leased_SiteTest() throws InterruptedException;
	
	//Steam
	
	public void Fuel_Consumed_for_Generating_Steam_Liquid_Owned_SiteTest() throws InterruptedException;
	public void Steam_Purchased_District_Grid_Heat_Owned_SiteTest() throws InterruptedException;
	public void Fuel_Consumed_for_Generating_Steam_Liquid_Leased_SiteTest() throws InterruptedException;
	public void Steam_Purchased_District_Grid_Steam_Leased_SiteTest() throws InterruptedException;
	public void Fuel_Consumed_for_Generating_Steam_Solid_Owned_SiteTest() throws InterruptedException;
    public void Fuel_Consumed_for_Generating_Steam_Solid_Leased_SiteTest() throws InterruptedException;
    
    public void delete_Fuel_Consumed_for_Generating_Steam_Liquid_Owned_SiteTest() throws InterruptedException;
	public void delete_Steam_Purchased_District_Grid_Heat_Owned_SiteTest() throws InterruptedException;
	public void delete_Fuel_Consumed_for_Generating_Steam_Liquid_Leased_SiteTest() throws InterruptedException;
	public void delete_Steam_Purchased_District_Grid_Steam_Leased_SiteTest() throws InterruptedException;
	public void delete_Fuel_Consumed_for_Generating_Steam_Solid_Owned_SiteTest() throws InterruptedException;
    public void delete_Fuel_Consumed_for_Generating_Steam_Solid_Leased_SiteTest() throws InterruptedException;
	
	//Co-Generation
	public void Fuel_Consumed_for_Generating_Co_Gen_Liquid_Owned_SiteTest() throws InterruptedException;
	public void Fuel_Consumed_for_Generating_Co_Gen_Liquid_Leased_SiteTest() throws InterruptedException;
	public void Fuel_Consumed_for_Generating_Co_Gen_Solid_Owned_SiteTest() throws InterruptedException;
	public void Fuel_Consumed_for_Generating_Co_Gen_Solid_Leased_SiteTest() throws InterruptedException;
	
	public void delete_Fuel_Consumed_for_Generating_Co_Gen_Liquid_Owned_SiteTest() throws InterruptedException;
	public void delete_Fuel_Consumed_for_Generating_Co_Gen_Liquid_Leased_SiteTest() throws InterruptedException;
	public void delete_Fuel_Consumed_for_Generating_Co_Gen_Solid_Owned_SiteTest() throws InterruptedException;
	public void delete_Fuel_Consumed_for_Generating_Co_Gen_Solid_Leased_SiteTest() throws InterruptedException;
	

}
