package admin_testcases;

public interface TransportTestCases {
	//Road
	public void Road_By_Fuel_Owned_Operated_VehiclesTest() throws InterruptedException;
	public void Road_By_Fuel_Not_Owned_Operated_VehiclesTest() throws InterruptedException;
	public void Road_By_Distance_Owned_Operated_VehiclesTest() throws InterruptedException;
	public void Road_By_Distance_Not_Owned_Operated_VehiclesTest() throws InterruptedException;
	
	public void delete_Road_By_Fuel_Owned_Operated_VehiclesTest() throws InterruptedException;
	public void delete_Road_By_Fuel_Not_Owned_Operated_VehiclesTest() throws InterruptedException;
	public void delete_Road_By_Distance_Owned_Operated_VehiclesTest() throws InterruptedException;
	public void delete_Road_By_Distance_Not_Owned_Operated_VehiclesTest() throws InterruptedException;
	
	//Air
	public void addAirAssetTest();
	
	public void deleteAirAssetTest();

	//Public
	public void PublicTransportTest() throws InterruptedException;
	
	public void deletePublicTransportTest() throws InterruptedException;
}
