package database_test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import admin_pages.HomePage;

public class ConnectionTest extends HomePage{
	//private WebDriver driver = null;
	  private Connection con = null;
	  private Statement stmt = null;
	  String baseUrl = "http://162.254.26.105/TeslaUAT/Accounts";
	  /*@BeforeClass
	  public void setUp() throws Exception {
	  
	    driver = new FirefoxDriver();
	    baseUrl = "http://www.testexample.com";
	    driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	  }
*/
	  @Test
	  public void test() throws SQLException, ClassNotFoundException {
	  
	    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
	  
	    String url = "jdbc:sqlserver://162.254.26.105:1433;DatabaseName=UserInfo";
	
	    con = DriverManager.getConnection(url, "sa", "12345678");
	  
	    stmt = con.createStatement();
	   
	    try {
	      String query = "select * from userlogin";
	      ResultSet result = stmt.executeQuery(query);
	      if (result.next()) {
	        while (result.next()) {
	         
	         
	          String username = result.getString("username");
	          String password = result.getString("userpassword");
	       
	          System.out.println("username :" + username);
	          System.out.println("password: " + password);
	        }
	        result.close();
	      }
	    }

	    catch (SQLException ex) {
	      System.out.println(ex);
	    }
	
	    String newtestusername = "newuser";
	    String newtestuserpassword = "newuser";

	    driver.get(baseUrl + "Login.aspx");
	 
	    driver.findElement(By.id("userID")).sendKeys(newtestusername);
	   
	    driver.findElement(By.id("password")).sendKeys(newtestuserpassword);
	   
	    driver.findElement(By.id("AddUser")).click();
	 
	    System.out
	        .println("Is welcome message displayed: "
	            + isElementPresent(By
	                .xpath("//*[contains(.,'Welcome back ')]")));

	    String newuserquery = "SELECT * From userlogin where username=?";

	    PreparedStatement stat = con.prepareStatement(newuserquery);
	    stat.setString(1, newtestusername);
	    try {
	      boolean hasResultSet = stat.execute();
	      if (hasResultSet) {
	        ResultSet result = stat.getResultSet();
	   

	        String newusername = result.getString("username");
	  
	        Assert.assertEquals(newtestusername, newusername);
	      }
	    } catch (SQLException ex)

	    {
	      System.out.println(ex);
	    } finally {
	      con.close();
	    }

	  }

	  /*@AfterClass
	  public void tearDown() throws Exception {
	    // close the driver
	    driver.close();
	  }
*/
	  private boolean isElementPresent(By by) {
	    try {
	      driver.findElement(by);
	      return true;
	    } catch (NoSuchElementException e) {
	      return false;
	    }
	  }
	

}
