package ides_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.Select;

import admin_pages.HomePage;

public class IdesPage extends HomePage {
	private By elUserRoles=By.id("ctl00_ddlUserRoles");
	private By elFacility=By.id("ddlFacility");
	
	
	
	protected void selectUserRoles(String role) throws InterruptedException{
		try {
			Select s=new Select(driver.findElement(elUserRoles));
			s.selectByVisibleText(role);
			Thread.sleep(5000);
		} catch (NoSuchElementException e) {
		System.out.println("User role is not available");
			e.printStackTrace();
		}
	}
	protected void selectUserFacility(String facility) throws InterruptedException{
		try {
			Select s=new Select(driver.findElement(elFacility));
			s.selectByVisibleText(facility);
			Thread.sleep(5000);
		} catch (NoSuchElementException e) {
		System.out.println("Facility is not available");
			e.printStackTrace();
		}
	}

}
