package common;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class CommonElements{
	String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
	private By elFacility=By.id("ddlFacilities");
	private By elActivity=By.id("ddlActivityBase");
	private By elWeightUnit=By.id("ddlUnits");
	private By elAverageCost=By.id("txtAverageCost");
	private By eladdnewEF=By.id("btnAddNewEF");
	private By elAuthority=By.id("ddlAuthority");
	private By elCountry=By.id("ddlCountry");
	private By elState=By.id("ddlState");
	private By elYear=By.id("ddlEFYear");
	private By elNewAsset=By.id("assetBtn");
	private By elAssetName=By.id("txtAssetName");
	private By elCo2EF=By.id("txtCo2EFAdd");
	private By elCH4EF=By.id("txtCH4EFAdd");
	private By elN2OEF=By.id("txtN2OEFAdd");
	private By elReplicate=By.id("chkReplicateAllLocations");
	private By elNewEFSave=By.id("btnSaveNewEF");
	private By elCO2EFUnit=By.id("ddlCo2EFUnitAdd");
	private By elCH4EFUnit=By.id("ddlCH4EFUnitAdd");
	private By elN2OEFUnit=By.id("ddlN2OEFUnitAdd");
	private By elSaveAsset=By.id("btnSaveAsset");
	private By elAlertMsg=By.id("dialog-content-inner");
	private By elAlertOk=By.id("dialog-button");
	private By elAssetRow=By.xpath("//table/tbody[@role='rowgroup']/tr");
	private By elCheckDeleteAllAsset=By.id("chkAllDeleteAsset");
	private By elBtnDeleteAllAsset=By.id("btnDeleteAllAsset");
	private By elCO2AppliedEF=By.id("lblEFCo2App");
	private By elCH4AppliedEF=By.id("lblEFCH4App");
	private By elN2OAppliedEF=By.id("lblEFN2OApp");
	private By elEF_Error_Msg=By.id("lblEFErrMsg");
	private By elDuplicateAssetMsg=By.id("lblAssetErrorMsg");
	private By elCloseAssetWindow=By.xpath(".//*[@id='assetPopup']/div[2]//button[@class='close']");
	
	public WebDriver driver;
	
	public void menu(String mainMenu, String subMenu) throws InterruptedException{
		try {
			driver.findElement(By.xpath(".//*[@id='bs-example-navbar-collapse-1']/ul/li/a[contains(text(),'"+mainMenu+"')]/b[@class='caret']")).click();
			driver.findElement(By.xpath(".//*[@id='bs-example-navbar-collapse-1']/ul/li[@class='dropdown open']/ul/div//li/a[text()='"+subMenu+"']")).click();
		} catch (NoSuchElementException e) {
		System.out.println("Main menu is not loaded properly");
			e.printStackTrace();
		}
		Thread.sleep(5000);
	}
	protected void selectFacility(String facility) throws InterruptedException{
	
		try {
			Select f=new Select(driver.findElement(elFacility));
			f.selectByVisibleText(facility);
		} catch (NoSuchElementException e) {
		System.out.println("Facility is not available");
			e.printStackTrace();
		}
	}
	//common elements
		public void clickAddAsset() throws InterruptedException{
			driver.findElement(elNewAsset).click();
			Thread.sleep(5000);
		}
		public void assetName(String asset_Name){
			driver.findElement(elAssetName).sendKeys(asset_Name);
		}
	public void selectActivity(String activity) throws InterruptedException{
	
		try {
			Select f=new Select(driver.findElement(elActivity));
			f.selectByVisibleText(activity);
		} catch (NoSuchElementException e) {
			System.out.println("Activity is not available");
			e.printStackTrace();
		}
		Thread.sleep(5000);
	}
	
	public void avarageCost(String cost){
		driver.findElement(elAverageCost).sendKeys(cost);
	}
	public void clickAddNewEF() throws InterruptedException{
		driver.findElement(eladdnewEF).click();
		//Thread.sleep(5000);
	}
	public void addAuthority() throws InterruptedException{
	
		try {
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Select a=new Select(driver.findElement(elAuthority));
			a.selectByVisibleText("DEFRA");
		} catch (NoSuchElementException e) {
			System.out.println("Authority is not available");
			e.printStackTrace();
		}
		Thread.sleep(3000);
		
		try {
			Select c=new Select(driver.findElement(elCountry));
			c.selectByVisibleText("Canada");
		} catch (NoSuchElementException e) {
			System.out.println("Country is not available");
			e.printStackTrace();
		}
		Thread.sleep(3000);
	
		try {
			Select s=new Select(driver.findElement(elState));
			s.selectByVisibleText("Ontario");
		} catch (NoSuchElementException e) {
			System.out.println("State is not available");
			e.printStackTrace();
		}
		Thread.sleep(3000);
	
		try {
			Select y=new Select(driver.findElement(elYear));
			y.selectByVisibleText("2016");
		} catch (NoSuchElementException e) {
			System.out.println("Year is not available");
			e.printStackTrace();
		}
		Thread.sleep(3000);
	}
	protected void selectUnit(String unit) throws InterruptedException{

		try {
			Select w=new Select(driver.findElement(elWeightUnit));	
			w.selectByVisibleText(unit);
		} catch (NoSuchElementException e) {
			System.out.println("Unit is not available");
			e.printStackTrace();
		}
		Thread.sleep(3000);
		}
	public void addCO2EF(String carbonEF){
		driver.findElement(elCo2EF).clear();
		driver.findElement(elCo2EF).sendKeys(carbonEF);
	}
	public void addCH4EF(String methaneEF){
		driver.findElement(elCH4EF).clear();
		driver.findElement(elCH4EF).sendKeys(methaneEF);
	}
	public void addN2OEF(String nitrousEF){
		driver.findElement(elN2OEF).clear();
		driver.findElement(elN2OEF).sendKeys(nitrousEF);
	}
	public void replicate(String y){
		
		
			try {
				WebElement	rpl = driver.findElement(elReplicate);
				if(y.equalsIgnoreCase("No")){
					if(rpl.isSelected()){
						rpl.click();
					}
				}else 
					if(!rpl.isSelected()){
						rpl.click();
					}
			} catch (Exception e) {
			System.out.println("There is no option to replicate for all facility");
				e.printStackTrace();
			}
		
			
		
	}
	public void saveNewEF() throws InterruptedException{
		driver.findElement(elNewEFSave).click();
		Thread.sleep(3000);
	}
	public void selectCO2EFUnit(String co2efUnit) throws InterruptedException{
	
		try {
			Select c=new Select(driver.findElement(elCO2EFUnit));
			c.selectByVisibleText(co2efUnit);
		} catch (NoSuchElementException e) {
		System.out.println("CO2 EF Unit is not available");
			e.printStackTrace();
		}
		Thread.sleep(3000);
	}
	public void selectCH4EFUnit(String ch4efUnit) throws InterruptedException{
		
		try {
			Select c=new Select(driver.findElement(elCH4EFUnit));
			c.selectByVisibleText(ch4efUnit);
		} catch (NoSuchElementException e) {
		System.out.println("CH4 EF Unit is not available");
			e.printStackTrace();
		}
		Thread.sleep(3000);
	}
	public void selectN2OEFUnit(String n2oefUnit) throws InterruptedException{
	
		try {
			Select c=new Select(driver.findElement(elN2OEFUnit));
			c.selectByVisibleText(n2oefUnit);
		} catch (NoSuchElementException e) {
		System.out.println("N2O EF Unit is not available");
			e.printStackTrace();
		}
		Thread.sleep(3000);
	}
	public void saveAsset() throws InterruptedException{
		WebElement element=driver.findElement(elSaveAsset);
	
		((JavascriptExecutor) driver).executeScript(
                "arguments[0].scrollIntoView();", element);
		element.click();
		Thread.sleep(5000);
		
	}
	public void acceptAlert() throws InterruptedException{
		try {
			System.out.println(driver.findElement(elAlertMsg).getText());
			driver.findElement(elAlertOk).click();
			Thread.sleep(5000);
		} catch (NoSuchElementException e) {
		System.out.println("No alert message available");
			e.printStackTrace();
		}
	}
	public void pageReload() throws InterruptedException{
		driver.navigate().refresh();
		Thread.sleep(5000);
	}
	public void deleteAsset() throws InterruptedException{
		if(driver.findElements(elAssetRow).size()!=0){
			WebElement element=driver.findElement(elCheckDeleteAllAsset);
			((JavascriptExecutor) driver).executeScript(
	                "arguments[0].scrollIntoView();", element);
			element.click();
			Thread.sleep(5000);
			driver.findElement(elBtnDeleteAllAsset).click();
			Thread.sleep(5000);
			Alert alert=driver.switchTo().alert();
			alert.accept();
			
		}
	}
	protected void verifyEF(){
		driver.findElement(By.xpath("//table/tbody/tr[1]/td[8]/a")).click();
		try {
			Thread.sleep(5000);
			String co2ef=driver.findElement(elCO2AppliedEF).getText();
			//String co2ef1=co2ef.replaceAll("[^\\.0123456789]","");
			String co2ef1=co2ef.replaceAll(" .*", "");
			System.out.println(co2ef1);
			String ch4ef=driver.findElement(elCH4AppliedEF).getText();
			String ch4ef1=ch4ef.replaceAll(" .*", "");
			String n2oef=driver.findElement(elN2OAppliedEF).getText();
			String n2oef1=n2oef.replaceAll(" .*", "");
			
		} catch (InterruptedException e) {
			System.out.println("Applied EF has not loaded");
			e.printStackTrace();
		}
		
				
		
	}
	protected void verifyAsset(String asset_Name, String facility) throws InterruptedException{
		List<WebElement> assets=driver.findElements(By.xpath(".//*[@id='tblDataAdd']//table/tbody/tr"));
		if(assets.size()>0){
			for (int i = 1; i <= assets.size(); i++) {
				String asset=driver.findElement(By.xpath(".//*[@id='tblDataAdd']//table/tbody/tr["+i+"]/td[2]")).getText();
				String f=driver.findElement(By.xpath(".//*[@id='tblDataAdd']//table/tbody/tr["+i+"]/td[1]")).getText();
				Thread.sleep(2000);
				if(asset.equalsIgnoreCase(asset_Name)&&f.equalsIgnoreCase(facility)){
					Assert.assertTrue(true, "Asset has not been created");
				}
				
			}
		}
	}
	protected void getscreenshot(String fname) throws Exception 
    {
            File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
         //The below method will save the screen shot in d drive with name "screenshot.png"
            FileUtils.copyFile(scrFile, new File("D:\\"+fname+timeStamp+".png"));
    }
	protected boolean isErrorMsgDisplayed() throws InterruptedException{
		WebElement eMsg=driver.findElement(elEF_Error_Msg);
	    String s=eMsg.getText();
	    boolean b=s.contains("Emission Factor");
	    return b;
	}
	protected boolean isAssetDuplicate() throws InterruptedException{
		WebElement eMsg=driver.findElement(elDuplicateAssetMsg);
		 String s=eMsg.getText();
		    boolean b=s.contains("Duplicate");
		    return b;
	}
	protected void closeAssetWindow(){
		WebElement element =driver.findElement(elCloseAssetWindow);
		((JavascriptExecutor) driver).executeScript(
                "arguments[0].scrollIntoView();", element);
		element.click();
	}
}
