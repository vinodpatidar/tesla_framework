package ides_tests;

import ides_pages.IdesPage;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

import admin_pages.HomePage;

public class UserLogin extends IdesPage {
	private By txt_id=By.id("ctl00_ContentPlaceHolder1_txtUserName");
	private By txt_pwd=By.id("ctl00_ContentPlaceHolder1_txtPass");
	private By login=By.id("ctl00_ContentPlaceHolder1_btnLogin");
	
	
	protected void enterid(String id){
	try {
		driver.findElement(txt_id).sendKeys(id);
	} catch (NoSuchElementException e) {
	System.out.println("Page has not loaded");
		e.printStackTrace();
	}
		
	}
	protected void enterPwd(String pwd){
		try {
			driver.findElement(txt_pwd).sendKeys(pwd);
		} catch (NoSuchElementException e) {
			System.out.println("Page has not loaded");
			e.printStackTrace();
		}
			
		}
	protected void login(){
		try {
			driver.findElement(login).click();
		} catch (NoSuchElementException e) {
			System.out.println("Page has not loaded");
			e.printStackTrace();
		}
			
		}
	@BeforeTest
	protected void userlogin(){
		enterid("teslauser2@gmail.com");
		enterPwd("binary@1");
		login();
	}
	@AfterTest
	protected void logout(){
		driver.navigate().refresh();
		driver.findElement(By.xpath("//div[@id='bs-example-navbar-collapse-1']//div[@class='logout']/a[contains(text(),'Logout')]")).click();
		//driver.close();
	}
}
