package ides_tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class IDESOperation extends UserLogin {
	@Test
	public void dataEntryTest() throws InterruptedException{
		XSSFSheet sheet=wb.getSheet("ides");
		Logger log=Logger.getLogger("IDES data approval test");
		PropertyConfigurator.configure("./log/log4j.properties");	
		menu("Data","Contributor Data Entry");
		selectUserRoles("Contributor");
		selectUserFacility("YUMA PLANT");
		List<WebElement> variables=driver.findElements(By.xpath("//div[@id='gridIDESDataEntry']//tbody[@role='rowgroup']/tr"));
		log.info("Total variable are: "+" "+variables.size());
		if(variables.size()>=1){
			for(int i=1;i<=variables.size();i++){
				WebElement elmt=driver.findElement(By.xpath("//div[@id='gridIDESDataEntry']//tbody[@role='rowgroup']/tr["+i+"]/td/label[contains(@id,'lblVariableName')]"));
			log.info("Data Entry in variable: "+" "+elmt.getText());
			driver.findElement(By.xpath("//div[@id='gridIDESDataEntry']//tbody[@role='rowgroup']/tr["+i+"]/td/input[contains(@id,'txtQuantity_')]")).sendKeys(sheet.getRow(7).getCell(5).getStringCellValue());
			log.info("Enter qualtity");
			driver.findElement(By.xpath("//div[@id='gridIDESDataEntry']//tbody[@role='rowgroup']/tr["+i+"]/td//span/input[contains(@id,'txtDateFrom_')]")).click();
			
			driver.findElement(By.xpath("//div[@id='gridIDESDataEntry']//tbody[@role='rowgroup']/tr["+i+"]/td//span/input[contains(@id,'txtDateFrom_')]")).sendKeys(sheet.getRow(8).getCell(5).getStringCellValue());
			Thread.sleep(5000);
			log.info("Enter From date");
			driver.findElement(By.xpath("//div[@id='gridIDESDataEntry']//tbody[@role='rowgroup']/tr["+i+"]/td//span/input[contains(@id,'txtDateTo_')]")).sendKeys(sheet.getRow(9).getCell(5).getStringCellValue());
			Thread.sleep(5000);
			log.info("Enter To date");
			driver.findElement(By.xpath("//div[@id='gridIDESDataEntry']//tbody[@role='rowgroup']/tr["+i+"]/td/input[contains(@id,'txtNotes_')]")).sendKeys(sheet.getRow(10).getCell(5).getStringCellValue());
			Thread.sleep(5000);
			log.info("Enter text note");
		//driver.findElement(By.xpath("//div[@id='gridIDESDataEntry']//tbody[@role='rowgroup']/tr["+i+"]/td[@role='gridcell']/div[@class='fileUpload']")).sendKeys(sheet.getRow(11).getCell(5).getStringCellValue());
		//	driver.findElement(By.xpath("//div[@id='gridIDESDataEntry']//tbody[@role='rowgroup']/tr/td[@role='gridcell']/div[@class='fileUpload']")).sendKeys("C:\\TestData\\DataEntry.doc");
			//Thread.sleep(10000);
			//log.info("File uploaded");
			driver.findElement(By.xpath("//div[@id='gridIDESDataEntry']//tbody[@role='rowgroup']/tr["+i+"]/td[@role='gridcell']/input[contains(@id,'btnSubmit_')]")).click();
			Thread.sleep(5000);
			log.info("Click on save button");
			Thread.sleep(5000);
			log.info(driver.findElement(By.id("dialog-content-inner")));
			driver.findElement(By.id("dialog-button")).click();
			
			Thread.sleep(10000);
			}
			//verify data entry
			log.info("Data verification");
			driver.findElement(By.xpath(".//*[@id='bs-example-navbar-collapse-1']/ul/li/a[contains(text(),'Data')]/b[@class='caret']")).click();
			driver.findElement(By.xpath(".//*[@id='bs-example-navbar-collapse-1']/ul/li[@class='dropdown open']/ul/div//li/a[text()='Entered Data']")).click();
			Thread.sleep(10000);
			//need to implement
			
			
		}else{
			log.error("There is no variable associated with selected facility");
		}
		
	}
	@Test(description="Data approval test")
	public void approveDataTest() throws InterruptedException{
		XSSFSheet sheet=wb.getSheet("ides");
		Logger log=Logger.getLogger("IDES data approval test");
		PropertyConfigurator.configure("./log/log4j.properties");	
		//login as contributor
		menu("Data","Data Approval");
	
		
		List<WebElement> aproval=driver.findElements(By.xpath(".//*[@id='gvEnteredDataList']//table/tbody/tr"));
		if(aproval.size()>0){
			for (int i = 1; i <=aproval.size(); i++) {
				driver.findElement(By.xpath(".//*[@id='gvEnteredDataList']//table/tbody/tr["+i+"]/td[2]/a")).click();
				Thread.sleep(10000);
				driver.findElement(By.id("checkAll")).click();
				Thread.sleep(5000);
				driver.findElement(By.id("txtComments")).sendKeys("Test data approval comments");
				driver.findElement(By.id("btnApprove")).click();
				Thread.sleep(5000);
				log.info(driver.findElement(By.id("dialog-content-inner")).getText());
				driver.findElement(By.id("dialog-button")).click();
				Thread.sleep(5000);
				driver.findElement(By.xpath(".//*[@id='bs-example-navbar-collapse-1']/ul/li/a[contains(text(),'Data')]/b[@class='caret']")).click();
				driver.findElement(By.xpath(".//*[@id='bs-example-navbar-collapse-1']/ul/li[@class='dropdown open']/ul/div//li/a[text()='Data Approval']")).click();
				Thread.sleep(10000);
				aproval=driver.findElements(By.xpath(".//*[@id='gvEnteredDataList']//table/tbody/tr"));
				
				
			}
			
			
		}
		
		
		
	}
	@Test(description="Data reject test")
	public void rejectDataTest() throws InterruptedException{
		XSSFSheet sheet=wb.getSheet("ides");
		Logger log=Logger.getLogger("IDES data reject test");
		PropertyConfigurator.configure("./log/log4j.properties");	
		//login as contributor
		Select contributor=new Select(driver.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_ddlFacility_Roles']")));
		contributor.selectByVisibleText(sheet.getRow(16).getCell(5).getStringCellValue());
		Thread.sleep(8000);
		log.info("Login as validator");
		driver.findElement(By.xpath(".//*[@id='bs-example-navbar-collapse-1']/ul/li/a[contains(text(),'Data')]/b[@class='caret']")).click();
		driver.findElement(By.xpath(".//*[@id='bs-example-navbar-collapse-1']/ul/li[@class='dropdown open']/ul/div//li/a[text()='Data Approval']")).click();
		Thread.sleep(10000);
		
		List<WebElement> aproval=driver.findElements(By.xpath(".//*[@id='gvEnteredDataList']//table/tbody/tr"));
		if(aproval.size()>0){
			for (int i = 1; i <=aproval.size(); i++) {
				driver.findElement(By.xpath(".//*[@id='gvEnteredDataList']//table/tbody/tr["+i+"]/td[2]/a")).click();
				Thread.sleep(10000);
				driver.findElement(By.id("checkAll")).click();
				Thread.sleep(5000);
				driver.findElement(By.id("txtComments")).sendKeys("Test data reject comments");
				driver.findElement(By.id("btnReject")).click();
				Thread.sleep(5000);
				log.info(driver.findElement(By.id("dialog-content-inner")).getText());
				driver.findElement(By.id("dialog-button")).click();
				Thread.sleep(5000);
				driver.findElement(By.xpath(".//*[@id='bs-example-navbar-collapse-1']/ul/li/a[contains(text(),'Data')]/b[@class='caret']")).click();
				driver.findElement(By.xpath(".//*[@id='bs-example-navbar-collapse-1']/ul/li[@class='dropdown open']/ul/div//li/a[text()='Data Approval']")).click();
				Thread.sleep(10000);
				aproval=driver.findElements(By.xpath(".//*[@id='gvEnteredDataList']//table/tbody/tr"));
				
				
			}
			
			
		}
		
	}
@Test
public void dataEntryFromExcel() throws InterruptedException, IOException{
	
	 FileInputStream file = new FileInputStream(new File("D:\\Approved Data Report.xls"));
		
	//Get the workbook instance for XLS file 
	HSSFWorkbook workbook = new HSSFWorkbook(file);

	//Get first sheet from the workbook
	HSSFSheet sheet = workbook.getSheet("Sheet1");
	System.out.println(sheet.getRow(3).getCell(4));
	
	Logger log=Logger.getLogger("IDES data approval test");
	PropertyConfigurator.configure("./log/log4j.properties");	
	menu("Data","Contributor Data Entry");
	selectUserRoles("Contributor");
	selectUserFacility("NEW JERSEY PLANT");
	
		for(int i=193;i<=203;i++){
			WebElement elmt=driver.findElement(By.xpath("//div[@id='gridIDESDataEntry']//tbody[@role='rowgroup']/tr/td/label[contains(@id,'lblVariableName')]"));
		log.info("Data Entry in variable: "+" "+elmt.getText());
		DataFormatter formatter = new DataFormatter(); //creating formatter using the default locale
		 Cell cell = sheet.getRow(i).getCell(4);
		 String value = formatter.formatCellValue(cell);
		driver.findElement(By.xpath("//div[@id='gridIDESDataEntry']//tbody[@role='rowgroup']/tr/td/input[contains(@id,'txtQuantity_')]")).sendKeys(value);
		log.info("Enter qualtity");
		driver.findElement(By.xpath("//div[@id='gridIDESDataEntry']//tbody[@role='rowgroup']/tr/td//span/input[contains(@id,'txtDateFrom_')]")).click();
		
		driver.findElement(By.xpath("//div[@id='gridIDESDataEntry']//tbody[@role='rowgroup']/tr/td//span/input[contains(@id,'txtDateFrom_')]")).sendKeys(sheet.getRow(i).getCell(6).getStringCellValue());
		Thread.sleep(5000);
		log.info("Enter From date");
		driver.findElement(By.xpath("//div[@id='gridIDESDataEntry']//tbody[@role='rowgroup']/tr/td//span/input[contains(@id,'txtDateTo_')]")).sendKeys(sheet.getRow(i).getCell(7).getStringCellValue());
		Thread.sleep(5000);
		log.info("Enter To date");
	//	driver.findElement(By.xpath("//div[@id='gridIDESDataEntry']//tbody[@role='rowgroup']/tr["+i+"]/td/input[contains(@id,'txtNotes_')]")).sendKeys(sheet.getRow(10).getCell(5).getStringCellValue());
		Thread.sleep(5000);
	//	log.info("Enter text note");
	//driver.findElement(By.xpath("//div[@id='gridIDESDataEntry']//tbody[@role='rowgroup']/tr["+i+"]/td[@role='gridcell']/div[@class='fileUpload']")).sendKeys(sheet.getRow(11).getCell(5).getStringCellValue());
	//	driver.findElement(By.xpath("//div[@id='gridIDESDataEntry']//tbody[@role='rowgroup']/tr/td[@role='gridcell']/div[@class='fileUpload']")).sendKeys("C:\\TestData\\DataEntry.doc");
		//Thread.sleep(10000);
		//log.info("File uploaded");
		driver.findElement(By.xpath("//div[@id='gridIDESDataEntry']//tbody[@role='rowgroup']/tr/td[@role='gridcell']/input[contains(@id,'btnSubmit_')]")).click();
		Thread.sleep(5000);
		log.info("Click on save button");
		Thread.sleep(5000);
		try {
			Alert alert =driver.switchTo().alert();
			alert.accept();
		} catch (NoAlertPresentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info(driver.findElement(By.id("dialog-content-inner")));
		driver.findElement(By.id("dialog-button")).click();
		
		Thread.sleep(8000);
		}
		
		
		
	}
	
}

